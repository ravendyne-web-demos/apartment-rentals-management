import os
import sys

_CFG_LOCATION = os.path.dirname( os.path.realpath( __file__ ) )
sys.path.append( os.path.normpath( '{}/tools'.format( _CFG_LOCATION ) ) )



##################################################################
#
# Build configuration
#
##################################################################

template_source = './build/site/js/app'
template_target = './jssrc/app'


source_target = './build/site/js/arm-app.js'
source_base = './jssrc/app/'

sources = [

    'apartment/arm-apartment.module.js',
    'apartment/arm-apartment-view.component.js',
    'apartment/arm-apartment-list.component.js',
    'apartment/arm-apartment-detail.component.js',
    'apartment/arm-apartment-filter.component.js',

    'services/arm-services.module.js',
    'services/arm-users.service.js',
    'services/arm-auth.service.js',
    'services/arm-apartments.service.js',
    'services/arm-system.service.js',

    'admin/arm-admin.module.js',
    'admin/arm-admin-dash.component.js',
    'admin/arm-users-list.component.js',
    'admin/arm-user-detail.component.js',

    'common/common.module.js',
    'common/arm-map.directive.js',
    'common/flash-message.component.js',
    'common/top-card.component.js',
    'common/bottom-card.component.js',
    'common/user-info.component.js',

    'app-auth/app-auth.module.js',
    'app-auth/app-login.component.js',
    'app-auth/app-register.component.js',
    'app-auth/dash.component.js',

    'sandbox/sandbox.module.js',
    'sandbox/sandbox.stuff.js',

    'arm-app.module.js',
    'arm-app.config.js',
]
