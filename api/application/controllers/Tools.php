<?php
/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */
defined('BASEPATH') OR exit('No direct script access allowed');

class Tools extends CI_Controller {

	public function index()
	{
		if( ! is_cli() ) {
			return;
		}

		echo "
HEH Tools:

register_user <username> <password> <email> <realtor/client> - register new user, use '%40' instead of '@' in email
dump_users - print 'client' and 'realtor' users in JSON format to STDOUT
load_users <file_name> - load 'client' and 'realtor' users from JSON file

";
		echo '';
	}

	// you have to use %40 instead of @, i.e.
	// php index.php tools register_user new_user le_password my%40email.com client
	public function register_user( $username, $password, $email, $group_name ) {

		$groups = '';

		if( $group_name == 'client' ) $groups = ['2'];
		else if( $group_name == 'realtor' ) $groups = ['3'];
		else {
			print "Please specify either 'client' or 'mamanger' for user's group";
			return;
		}

		$additional_data = [];
		$user_id = $this->ion_auth->register( $username, $password, rawurldecode( $email ), $additional_data, $groups );

		if( $user_id === false ) {
            print $this->ion_auth->errors();
        }
	}

	// php index.php tools dump_users > users.json
	public function dump_users()
	{
		if( ! is_cli() ) {
			return;
		}

        $user_list = $this->ion_auth->users( '2' )->result();
		$user_list = array_map( function( $el ) {
			return array(
				'id' => $el->user_id,
				'username' => $el->username,
				'email' => $el->email,
				'realtor' => false
			);
		}, $user_list );
        $realtors_user_list = $this->ion_auth->users( '3' )->result();
		$realtors_user_list = array_map( function( $el ) {
			return array(
				'id' => $el->user_id,
				'username' => $el->username,
				'email' => $el->email,
				'realtor' => true
			);
		}, $realtors_user_list );
		$user_list = array_merge( $user_list, $realtors_user_list );

		echo json_encode( $user_list );
	}

	// php index.php tools load_users users.json
	public function load_users( $file_name ) {

		if( ! is_cli() ) {
			return;
		}

		if( ! $file_name ) {
			return;
		}

		// $jsonArray = json_decode( $this->input->raw_input_stream, true );
		$content = file_get_contents( $file_name );
		$jsonArray = json_decode( $content, true );
		if( ! $jsonArray ) {
			echo 'a JSON error here';
			return;
		}

		print_r($jsonArray);

	}
}
