<?php
/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */
defined('BASEPATH') OR exit('No direct script access allowed');

class Sandbox extends CI_Controller {

	public function index()
	{
		$response = [
			'success' => true,
		];

		$this->load->library('email');
		
		$this->email->from('your@example.com', 'Your Name');
		$this->email->to('someone@example.com');
		$this->email->cc('another@another-example.com');
		$this->email->bcc('them@their-example.com');
		
		$this->email->subject('Email Test');
		$this->email->message('Testing the email class.');

		if( ! $this->email->send() ) {
			$response['success'] = false;
		}

		// header('Content-Type: application/json');
		// echo json_encode( $response );
		echo $this->email->print_debugger();
	}
}
