<?php
/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends API_Controller {

	protected function get_send_invitation( $params ) {

        if( ! $this->site_auth_service->check_access( 'users' ) ) {
            $this->send_response( ['Unauthorized'], 401 );
            return;
        }

        $email = count( $params ) > 0 ? $params[0] : '';
		if( empty( $email ) ) {
            $this->send_response( ['Not implemented'], 405 );
            return;
        }

        $request['email'] = urldecode( $email );

        $response = array('success' => true );
		$response['email_debug'] = $this->site_user_service->send_invitation_email( $request );

		$this->send_response( $response );
	}

	protected function post() {

        if( ! $this->site_auth_service->check_access( 'users' ) ) {
            $this->send_response( ['Unauthorized'], 401 );
            return;
        }

		$request = $this->parse_request();
		if( $request === false ) return;

		$response = array( 'success' => false );

		if( array_key_exists( 'realtor', $request ) && $request['realtor'] === true ) {
			$response = $this->site_user_service->register_site_realtor( $request );
		} else {
			$response = $this->site_user_service->register_site_client( $request );
		}

        if( $response['success'] == false ) {

            $this->send_response( $response, 404 );
            return;
        }

        if( $response['success'] == true ) {

            $user_data = $response['user_data'];
            unset( $response['user_data'] );
            $response['location'] = '/users'.'/'.$user_data['id'];
            $response['activation'] = '/auth/activate'.'/'.$user_data['id'].'/'.$user_data['activation'];

            $this->send_response( $response, 201 );
            return;
        }

		$this->send_response( $response );
    }

	protected function post_avatar() {

        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'gif|jpg|jpeg|png';
        $config['max_size']             = 300;
        $config['max_width']            = 1024;
        $config['max_height']           = 1024;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('avatar'))
        {
            $error = array('error' => $this->upload->display_errors());

            $this->send_response( $error, 400 );
            return;
        }

        $upload_data = $this->upload->data();

        $response = $this->site_user_service->upload_photo_for_current_user( $upload_data );

		$this->send_response( $response );
    }

	protected function put( $params ) {

        if( ! $this->site_auth_service->check_access( 'users' ) ) {
            $this->send_response( ['Unauthorized'], 401 );
            return;
        }

        $id = count( $params ) > 0 ? $params[0] : '';
        if( empty( $id ) ) {
            $this->send_response( ['Not implemented'], 405 );
            return;
        }

		$request = $this->parse_request();
		if( $request === false ) return;

        $request['id'] = $id;
		$response = $this->site_user_service->update_site_user( $request );

        if( $response['success'] == false ) {

            $this->send_response( $response, 404 );
            return;
        }

		$this->send_response( $response );
	}

    protected function get( $params )
    {
        if( ! $this->site_auth_service->check_access( 'users' ) ) {
            $this->send_response( ['Unauthorized'], 401 );
            return;
        }

        $id = count( $params ) > 0 ? $params[0] : '';
        if( empty( $id ) ) {
            $response = $this->site_user_service->retrieve_user_list( [] );
            $this->send_response( $response );
            return;
        }

        $request = $this->parse_request( true );
		if( $request === false ) return;

        $request['id'] = $id;
        $response = $this->site_user_service->retrieve_user( $request );

        if( $response['success'] == false ) {

            $this->send_response( $response, 404 );
            return;
        }

		$this->send_response( $response );
    }

    protected function get_realtors()
    {
        if( ! $this->site_auth_service->check_access( 'users', 'get_realtors' ) ) {
            $this->send_response( ['Unauthorized'], 401 );
            return;
        }

        $request = $this->parse_request( true );
		if( $request === false ) return;

        $response = $this->site_user_service->retrieve_user_list( $request );
        $user_list = $response['users'];

        $user_list = array_filter( $user_list, function( $el ) {
            return $el['realtor'];
        } );
        // we have to do this here because array_filter() PRESERVES KEYS
        // which may result in $user_list being converted to JSON OBJECT {} instead of
        // JSON ARRAY [] when sent to API client.
        $response['users'] = array_values( $user_list );

		$this->send_response( $response );
    }

    protected function delete( $params )
    {
        if( ! $this->site_auth_service->check_access( 'users' ) ) {
            $this->send_response( ['Unauthorized'], 401 );
            return;
        }

        $id = count( $params ) > 0 ? $params[0] : '';
        if( empty( $id ) ) {
            $this->send_response( ['Not implemented'], 405 );
            return;
        }

		$request = $this->parse_request( true );
		if( $request === false ) return;

        $request['id'] = $id;
        $response = $this->site_user_service->deactivate_user( $request );

        if( $response['success'] == false ) {

            $this->send_response( $response, 404 );
            return;
        }

        $request = array( 'id' => $id );
        $this->site_apartment_service->delete_apartments_for_realtor( $request );

        $this->send_response( $response );
    }
}
