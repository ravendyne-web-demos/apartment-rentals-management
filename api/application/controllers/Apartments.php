<?php
/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */
defined('BASEPATH') OR exit('No direct script access allowed');

class Apartments extends API_Controller {

    // CREATE
    public function post()
    {
        if( ! $this->site_auth_service->check_access( 'apartments' ) ) {
            $this->send_response( ['Unauthorized'], 401 );
            return;
        }

		$request = $this->parse_request();
        if( $request === false ) return;

		$response = $this->site_apartment_service->create_apartment( $request );

        if( $response['success'] == true ) {

            $response['location'] = '/apartments'.'/'.$response['data']['id'];
            $this->send_response( $response, 201 );
            return;
        }

        $this->send_response( $response );
    }

    // RETRIEVE
    public function get( $params )
    {
        if( ! $this->site_auth_service->check_access( 'apartments' ) ) {
            $this->send_response( ['Unauthorized'], 401 );
            return;
        }

        $request = array();

        $id = count( $params ) > 0 ? $params[0] : '';
        if( ! empty( $id ) ) {
            $request['id'] = $id;
        }

        $response = $this->site_apartment_service->retrieve_apartment( $request );

        if( $response['success'] == false ) {

            $this->send_response( $response, 404 );
            return;
        }

		$this->send_response( $response );
    }

    public function get_filtered( $params )
    {
        if( ! $this->site_auth_service->check_access( 'apartments' ) ) {
            $this->send_response( ['Unauthorized'], 401 );
            return;
        }

        $filter = $this->parse_segments_to_dictionary( $params );

        $response = $this->site_apartment_service->retrieve_apartment( ['filter' => $filter] );

        if( $response['success'] == false ) {

            $this->send_response( $response, 404 );
            return;
        }

		$this->send_response( $response );
    }

    // UPDATE
    public function put( $params )
    {
        if( ! $this->site_auth_service->check_access( 'apartments' ) ) {
            $this->send_response( ['Unauthorized'], 401 );
            return;
        }

        $id = count( $params ) > 0 ? $params[0] : '';
        if( empty( $id ) ) {
            $this->send_response( ['Not implemented'], 405 );
            return;
        }

		$request = $this->parse_request();
		if( $request === false ) return;

        $request['id'] = $id;
        $response = $this->site_apartment_service->update_apartment( $request );

        if( $response['success'] == false ) {

            $this->send_response( $response, 404 );
            return;
        }

		$this->send_response( $response );
    }

    // DELETE
    public function delete( $params )
    {
        if( ! $this->site_auth_service->check_access( 'apartments' ) ) {
            $this->send_response( ['Unauthorized'], 401 );
            return;
        }

        $id = count( $params ) > 0 ? $params[0] : '';
        if( empty( $id ) ) {
            $this->send_response( ['Not implemented'], 405 );
            return;
        }

		$request = $this->parse_request( true );
		if( $request === false ) return;

        $request['id'] = $id;
        $response = $this->site_apartment_service->delete_apartment( $request );

        if( $response['success'] == false ) {

            $this->send_response( $response, 404 );
            return;
        }

		$this->send_response( $response );
    }
}
