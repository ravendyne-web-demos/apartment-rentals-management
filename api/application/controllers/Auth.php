<?php
/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends API_Controller {

	protected function get_whoami() {

		if( ! $this->ion_auth->logged_in() ) {
			$this->send_response( ['Unauthorized'], 401 );
			return;
		}

		$user = $this->ion_auth->user()->row();
        $response = $this->site_user_service->retrieve_user( array( 'id' => $user->user_id ) );

		$this->send_response( $response );
	}

	protected function post_register() {

		$request = $this->parse_request();
		if( $request === false ) return;

		$response = array( 'success' => false );

		$response = $this->site_user_service->register_site_client( $request );

		$this->send_response( $response );
	}

	protected function get_invite( $params ) {

        $email = count( $params ) > 0 ? $params[0] : '';
		if( empty( $email ) ) {
            header('Location: /site');
        }

        header('Location: /site/#!/register/' . urldecode( $email ));
	}

	// This should technically be POST request,
	// but we need to be email-friendly and implement GET instead
	// so users can just click a link and get activation done
	protected function get_activate( $params )
	{
        $id = count( $params ) > 0 ? $params[0] : '';
        $code = count( $params ) > 1 ? $params[1] : '';
		$activation = false;

		if( empty( $id ) || empty( $code ) ) {

			$this->send_response( ['Invalid request'], 400 );
			return;
		}

		$request = $this->parse_request( true );
		if( $request === false ) return;

		$request['id'] = $id;
		$request['code'] = $code;

		$response = $this->site_auth_service->activate( $request );

        if( $response['success'] ) {
            $this->show_flash_message('Activation successful.');
        } else {
            $this->show_flash_message('Account could not be activated.');
        }
	}

	protected function post_login()
	{
		$request = $this->parse_request();
		if( $request === false ) return;

		$response = $this->site_auth_service->login( $request );

		$this->send_response( $response );
	}

	protected function post_logout()
	{
		$request = $this->parse_request( true );
		if( $request === false ) return;

		$response = $this->site_auth_service->logout( $request );

		$this->send_response( $response );
    }

    //////////////////////////////////////////////////////////
    //
    // OAUTH PROVIDERS
    //
    //////////////////////////////////////////////////////////

	protected function get_oauth2( $params ) {

        $provider = count( $params ) > 0 ? $params[0] : '';

		if( empty( $provider ) ) {

            $this->show_flash_message( 'Invalid request' );
			// $this->send_response( ['Invalid request'], 400 );
			return;
        }

        switch( $provider ) {
            case 'github':
                $this->oauth2_login_github();
                return;
            case 'instagram':
                $this->oauth2_login_instagram();
                return;
            case 'google':
                $this->oauth2_login_google();
                return;
        }

        $this->show_flash_message( 'Invalid request' );
        // $this->send_response( ['Invalid request'], 400 );
    }

    //////////////////////////////////////////////////////////
    //
    // OAUTH GITHUB
    //
    //////////////////////////////////////////////////////////

	private function oauth2_login_github()
	{
        $this->config->load('arm', TRUE);
        $redirect_uri = $this->config->item('oauth2_redirect_uri', 'arm')['github'];

        $provider = new League\OAuth2\Client\Provider\Github([
            'clientId'          => 'db143105d05de779457b',
            'clientSecret'      => '42027928c15c58372acb2cccfbfe15ba5d69a705',
            'redirectUri'       => $redirect_uri,
            'scope'             => [],
        ]);

        $authUrl = $provider->getAuthorizationUrl();
        // save state to check against CSRF attack later in callback handler
        $this->session->set_userdata( 'oauth2_github_state', $provider->getState() );
        // redirect to OAuth2 provider
        header( 'Location: ' . $authUrl );
    }

    protected function get_oauthcallback_github() {

        $params = $this->uri->uri_to_assoc(3);

        if( empty($params['code']) || empty($params['state']) ) {
            $this->show_flash_message( 'Bad callback parameters' );
            return;
        }

        $this->config->load('arm', TRUE);
        $redirect_uri = $this->config->item('oauth2_redirect_uri', 'arm')['github'];

        $provider = new League\OAuth2\Client\Provider\Github([
            'clientId'          => 'db143105d05de779457b',
            'clientSecret'      => '42027928c15c58372acb2cccfbfe15ba5d69a705',
            'redirectUri'       => $redirect_uri,
            'scope'             => [],
        ]);

        $accessToken = $this->session->userdata('accessToken_github');

        if( ! $accessToken ) {

            try {

                // Try to get an access token (using the authorization code grant)
                $accessToken = $provider->getAccessToken('authorization_code', [
                    'code' => $params['code']
                ]);
                $this->session->set_userdata('accessToken_github', $accessToken);

            } catch (Exception $e) {

                $this->show_flash_message( 'There was an error processing login request' );
            }
        }


        // we have access token, one way or another
        $user = $provider->getResourceOwner($accessToken);
        $username = $user->getNickname();
        $email = $user->getEmail();
        if( ! $email ) {
            $email = $username . '@arm-github.oauth2';
        }

        // check if shadow user exists
        if( ! $this->site_user_service->exists( $email ) ) {
            // create shadow user for this one

            $avatar = $user->toArray()['avatar_url'];
            // $avatar = print_r( $user->toArray(), true );
            $default_avatar = $this->config->item('default_avatar', 'arm');
            $avatar = empty( $avatar ) ? $default_avatar : $avatar;

            $response = $this->site_user_service->register_oauth_account([
                'email' => $email,
                'username' => $username,
                'provider' => 'github',
                'picture' => $avatar,
            ]);

            if( ! $response['success'] ) {
                echo print_r($response, true);
                // $this->show_flash_message( 'There was an error during inital authentication via Github' );
                return;
            }
        }

        // login into our system using shadow user
        $password = $this->config->item('social_password', 'arm');
        $response = $this->site_auth_service->login([
            'identity' => $email,
            'password' => $password,
        ]);

        if( ! $response['success'] ) {
            echo print_r($response, true);
            // $this->show_flash_message( 'There was an error during login with your Github account' );
            return;
        }

        // done. we are logged in
        header('Location: /site/#!/dash');
    }

    //////////////////////////////////////////////////////////
    //
    // OAUTH GOOGLE
    //
    //////////////////////////////////////////////////////////

	private function oauth2_login_google()
	{
        $this->config->load('arm', TRUE);
        $redirect_uri = $this->config->item('oauth2_redirect_uri', 'arm')['google'];

        $provider = new League\OAuth2\Client\Provider\Google([
            'clientId'          => '290286996261-95msgpadmklndkrsek7megvfppoa3han.apps.googleusercontent.com',
            'clientSecret'      => 'bk7gLz_8IYm8c9TVLP7hFAIE',
            'redirectUri'       => $redirect_uri,
            'scope'             => [],
        ]);

        $authUrl = $provider->getAuthorizationUrl();
        // save state to check against CSRF attack later in callback handler
        $this->session->set_userdata( 'oauth2_google_state', $provider->getState() );
        // redirect to OAuth2 provider
        header( 'Location: ' . $authUrl );
    }

    protected function get_oauthcallback_google() {

        $params = $this->uri->uri_to_assoc(3);

        if( empty($params['code']) || empty($params['state']) ) {
            $this->show_flash_message( 'Bad callback parameters' );
            return;
        }

        $this->config->load('arm', TRUE);
        $redirect_uri = $this->config->item('oauth2_redirect_uri', 'arm')['google'];

        $provider = new League\OAuth2\Client\Provider\Google([
            'clientId'          => '290286996261-95msgpadmklndkrsek7megvfppoa3han.apps.googleusercontent.com',
            'clientSecret'      => 'bk7gLz_8IYm8c9TVLP7hFAIE',
            'redirectUri'       => $redirect_uri,
            'scope'             => [],
        ]);

        $accessToken = $this->session->userdata('accessToken_google');

        if( ! $accessToken ) {

            try {

                // Try to get an access token (using the authorization code grant)
                $accessToken = $provider->getAccessToken('authorization_code', [
                    'code' => urldecode( $params['code'] )
                ]);
                $this->session->set_userdata('accessToken_google', $accessToken);

            } catch (Exception $e) {

                $this->show_flash_message( 'There was an error processing login request' );
            }
        }


        // we have access token, one way or another
        $user = $provider->getResourceOwner($accessToken);
        $username = $user->getName();
        $email = $user->getEmail();
        if( ! $email ) {
            $email = $username . '@arm-google.oauth2';
        }

        // check if shadow user exists
        if( ! $this->site_user_service->exists( $email ) ) {
            // create shadow user for this one

            $avatar = $user->getAvatar();
            // $avatar = print_r( $user->toArray(), true );
            $default_avatar = $this->config->item('default_avatar', 'arm');
            $avatar = empty( $avatar ) ? $default_avatar : $avatar;

            $response = $this->site_user_service->register_oauth_account([
                'email' => $email,
                'username' => $username,
                'provider' => 'google',
                'picture' => $avatar,
            ]);

            if( ! $response['success'] ) {
                echo print_r($response, true);
                // $this->show_flash_message( 'There was an error during inital authentication via Github' );
                return;
            }
        }

        // login into our system using shadow user
        $password = $this->config->item('social_password', 'arm');
        $response = $this->site_auth_service->login([
            'identity' => $email,
            'password' => $password,
        ]);

        if( ! $response['success'] ) {
            echo print_r($response, true);
            // $this->show_flash_message( 'There was an error during login with your Github account' );
            return;
        }

        // done. we are logged in
        header('Location: /site/#!/dash');
    }

    //////////////////////////////////////////////////////////
    //
    // OAUTH INSTAGRAM
    //
    //////////////////////////////////////////////////////////

	private function oauth2_login_instagram()
	{
        $this->config->load('arm', TRUE);
        $redirect_uri = $this->config->item('oauth2_redirect_uri', 'arm')['instagram'];

        $provider = new League\OAuth2\Client\Provider\Instagram([
            'clientId'          => '087d189790bf4808826b7c96408483b5',
            'clientSecret'      => '2bff854ebb664d5fb0fae7458564edbf',
            'redirectUri'       => $redirect_uri,
            'scope'             => [],
        ]);

        $authUrl = $provider->getAuthorizationUrl();
        // save state to check against CSRF attack later in callback handler
        $this->session->set_userdata( 'oauth2_instagram_state', $provider->getState() );
        // redirect to OAuth2 provider
        header( 'Location: ' . $authUrl );
    }

    protected function get_oauthcallback_instagram() {

        $params = $this->uri->uri_to_assoc(3);

        if( empty($params['code']) || empty($params['state']) ) {
            $this->show_flash_message( 'Bad callback parameters' );
            return;
        }

        $this->config->load('arm', TRUE);
        $redirect_uri = $this->config->item('oauth2_redirect_uri', 'arm')['instagram'];

        $provider = new League\OAuth2\Client\Provider\Instagram([
            'clientId'          => '087d189790bf4808826b7c96408483b5',
            'clientSecret'      => '2bff854ebb664d5fb0fae7458564edbf',
            'redirectUri'       => $redirect_uri,
            'scope'             => [],
        ]);

        $accessToken = $this->session->userdata('accessToken_instagram');

        if( ! $accessToken ) {

            try {

                // Try to get an access token (using the authorization code grant)
                $accessToken = $provider->getAccessToken('authorization_code', [
                    'code' => urldecode( $params['code'] )
                ]);
                $this->session->set_userdata('accessToken_instagram', $accessToken);

            } catch (Exception $e) {

                $this->show_flash_message( 'There was an error processing login request' );
            }
        }


        // we have access token, one way or another
        $user = $provider->getResourceOwner($accessToken);
        $username = $user->getNickname();
        // $email = $user->getEmail();
        // if( ! $email ) {
            $email = $username . '@arm-instagram.oauth2';
        // }

        // check if shadow user exists
        if( ! $this->site_user_service->exists( $email ) ) {
            // create shadow user for this one

            $avatar = $user->getImageurl();
            // $avatar = print_r( $user->toArray(), true );
            $default_avatar = $this->config->item('default_avatar', 'arm');
            $avatar = empty( $avatar ) ? $default_avatar : $avatar;

            $response = $this->site_user_service->register_oauth_account([
                'email' => $email,
                'username' => $username,
                'provider' => 'instagram',
                'picture' => $avatar,
            ]);

            if( ! $response['success'] ) {
                echo print_r($response, true);
                // $this->show_flash_message( 'There was an error during inital authentication via Github' );
                return;
            }
        }

        // login into our system using shadow user
        $password = $this->config->item('social_password', 'arm');
        $response = $this->site_auth_service->login([
            'identity' => $email,
            'password' => $password,
        ]);

        if( ! $response['success'] ) {
            echo print_r($response, true);
            // $this->show_flash_message( 'There was an error during login with your Github account' );
            return;
        }

        // done. we are logged in
        header('Location: /site/#!/dash');
    }

    private function show_flash_message( $message ) {
        header('Location: /site/#!/message/' . urlencode( $message ));
    }
}
