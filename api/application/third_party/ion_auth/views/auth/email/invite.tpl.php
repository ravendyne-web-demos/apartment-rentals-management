<html>
<body>
	<h1><?php echo sprintf(lang('email_invite_heading'), $email);?></h1>
	<p><?php echo sprintf(lang('email_invite_subheading'), anchor('auth/invite/'. urlencode( $email ), lang('email_invite_link')));?></p>
</body>
</html>
