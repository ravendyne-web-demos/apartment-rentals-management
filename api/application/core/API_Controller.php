<?php
/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */
defined('BASEPATH') OR exit('No direct script access allowed');

// REST API style base controller
class API_Controller extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function _remap( $first_segment, $other_segments = array() )
    {
        return $this->handle_request( $first_segment, $other_segments );
    }

    private function handle_request( $first_segment, $other_segments ) {

        $method = $this->input->method(true);

        if( $first_segment != 'index' && ! is_numeric( $first_segment ) ) {

            $fn_suffix = '_'.$first_segment;
            $params = $other_segments;

        } else {

            $fn_suffix = '';
            if( $first_segment != 'index' ) {

                $params = array_merge( [$first_segment], $other_segments );

            } else {

                $params = $other_segments;
            }
        }

        $fn_name = '';

        switch( $method ) {

            case 'GET': $fn_name = 'get'.$fn_suffix; break;
            case 'PUT': $fn_name = 'put'.$fn_suffix; break;
            case 'POST': $fn_name = 'post'.$fn_suffix; break;
            case 'DELETE': $fn_name = 'delete'.$fn_suffix; break;
        }

        if( method_exists( $this, $fn_name ) ) {

            return call_user_func_array( array( $this, $fn_name ), array( $params ) );
        }

        $this->send_response( [ $method, $fn_name, $first_segment, $other_segments, $params ], 405 );
        // set_status_header( 405 );
    }

    protected function parse_segments_to_dictionary( $segments ) {

        $result = array();

        $segments_len = count( $segments );
        for( $i=0; $i < $segments_len; $i+=2) { 

            $result[ $segments[ $i ] ] = $segments[ $i + 1 ];
        }

        return $result;
    }

    protected function parse_request( $allow_empty = false ) {

        $request = json_decode($this->input->raw_input_stream, true);

        if( $request == NULL && $allow_empty ) {
            return [];
        }

        $json_conversion_error = $this->decode_json_error();

        if( $json_conversion_error ) {

            $response = array( 'success' => false, 'message' => $json_conversion_error );

            $this->send_response( $response, 400 );

            return false;
        }

        if( ! $allow_empty && empty( $request ) ) {

            $response = array( 'success' => false, 'message' => 'empty request' );

            $this->send_response( $response, 400 );

            return false;
        }
        
        return $request;
    }

    protected function send_response( $response, $code = 200 ) {

        if( $code == 401 ) {
            $response = array_merge( $response, array( 'success' => false, 'needs_login' => ! $this->ion_auth->logged_in(), 'message' => 'not authorized' ) );
            // header( 'WWW-Authenticate', 'Basic realm="Apartment Rental Management"' );
        }

        if( empty( $response ) ) {
            set_status_header( 404 );
            $response = array( 'message' => '404 - not found' );
            return;
        }

        set_status_header( $code );
        header('Content-Type: application/json');
        if( $code == 201 && array_key_exists( 'location', $response ) ) {
            header( 'Location: '.$response['location'] );
        }
		echo json_encode( $response );
    }

    protected function decode_json_error() {

        switch( json_last_error() ) {
            case JSON_ERROR_NONE:
                return '';
            break;
            case JSON_ERROR_DEPTH:
                return 'The maximum stack depth has been exceeded ';
            break;
            case JSON_ERROR_STATE_MISMATCH:
                return 'Invalid or malformed JSON';
            break;
            case JSON_ERROR_CTRL_CHAR:
                return 'Unexpected control character found';
            break;
            case JSON_ERROR_SYNTAX:
                return 'Syntax error, malformed JSON';
            break;
            case JSON_ERROR_UTF8:
                return 'Malformed UTF-8 characters, possibly incorrectly encoded';
            break;
            case JSON_ERROR_RECURSION:
                return 'One or more recursive references in the value to be encoded';
            break;
            case JSON_ERROR_INF_OR_NAN:
                return 'One or more NAN or INF values in the value to be encoded';
            break;
            case JSON_ERROR_UNSUPPORTED_TYPE:
                return 'A value of a type that cannot be encoded was given';
            break;
            case JSON_ERROR_INVALID_PROPERTY_NAME:
                return 'A property name that cannot be encoded was given';
            break;
            case JSON_ERROR_UTF16:
                return 'Malformed UTF-16 characters, possibly incorrectly encoded';
            break;
		}
		
		return 'Unknown error';
    }
}
