<?php
/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */
defined('BASEPATH') OR exit('No direct script access allowed');

class Site_user_service {

    // protected $CI;

    public function __construct() {
    }

    // Enables the use of CI super-global without having to define an extra variable.
    public function __get($var) {
        return get_instance()->$var;
    }

    //////////////////////////////////////////////////////////
    //
    // CREATE
    //
    //////////////////////////////////////////////////////////

    public function register_site_client( $request ) {

        // $group = array('client');
        $groups = array('2'); // must use ID

        return $this->register( $request, $groups );
    }

    public function register_site_realtor( $request ) {

        // $group = array('realtor');
        $groups = array('3'); // must use ID

        return $this->register( $request, $groups );
    }

    public function register( $request, $groups ) {

        $response = array( 'success' => false );

        $this->config->load('arm', TRUE);

        $avatar = $this->config->item('default_avatar', 'arm');
        $username = '';
        $password = '';
        $email = '';
        $additional_data = array(
            'first_name' => '',
            'last_name' => '',
        );
        $send_email = true;

        if( array_key_exists( 'username', $request ) ) {
            $username = $request['username'];
        }
        if( array_key_exists( 'password', $request ) ) {
            $password = $request['password'];
        }
        if( array_key_exists( 'email', $request ) ) {
            $email = $request['email'];
        }
        if( array_key_exists( 'send_email', $request ) ) {
            $send_email = boolval( $request['send_email'] );
        }
        if( array_key_exists( 'picture', $request ) ) {
            $avatar = $request['picture'];
        }

        if( ! ( $username && $password && $email ) ) {

            $response['message'] = 'missing required registration parameters';
            return $response;
        }

        if( $this->ion_auth->username_check( $username ) ) {

            $response['message'] = 'username already taken';
            return $response;
        }

        if( $this->ion_auth->email_check( $email ) ) {

            $response['message'] = 'email already taken';
            return $response;
        }

        $additional_data['picture'] = $avatar;

        $user_data = $this->ion_auth->register( $username, $password, $email, $additional_data, $groups );
        if( $user_data === false ) {
            $response['message'] = $this->ion_auth->errors();
            return $response;
        }

        $response['success'] = true;
        $response['user_data'] = $user_data;

        if( $send_email ) {
            $response['email_debug'] = $this->send_activation_email( $user_data );
        } else {
            $this->ion_auth->update(
                $user_data['id'],
                [
                    'active' => 1,
                    'activation_selector' => NULL,
                    'activation_code' => NULL,
                ]
            );
        }

        return $response;
    }

    private function send_activation_email( $user_data ) {

        $this->load->library('email');

        $message = $this->load->view(
            $this->config->item('email_templates', 'ion_auth').$this->config->item('email_activate', 'ion_auth'),
            $user_data,
            true);

        $this->email->clear();
        $this->email->from( $this->config->item('admin_email', 'ion_auth'), $this->config->item('site_title', 'ion_auth') );
        $this->email->to( $user_data['email'] );
        $this->email->subject( $this->config->item('site_title', 'ion_auth') . ' - ' . $this->lang->line('email_activation_subject') );
        $this->email->message( $message );

        if ($this->email->send() === TRUE) {
            // redirect to index page
        }

        return $this->email->print_debugger();
    }

    public function send_invitation_email( $user_data ) {

        $this->load->library('email');

        $message = $this->load->view(
            $this->config->item('email_templates', 'ion_auth').$this->config->item('email_invite', 'ion_auth'),
            $user_data,
            true);

        $this->email->clear();
        $this->email->from( $this->config->item('admin_email', 'ion_auth'), $this->config->item('site_title', 'ion_auth') );
        $this->email->to( $user_data['email'] );
        $this->email->subject( $this->config->item('site_title', 'ion_auth') . ' - ' . $this->lang->line('email_invitation_subject') );
        $this->email->message( $message );

        if ($this->email->send() === TRUE) {
            // redirect to index page
        }

        return $this->email->print_debugger();
    }

    //////////////////////////////////////////////////////////
    //
    // UPDATE
    //
    //////////////////////////////////////////////////////////

    public function update_site_user( $request ) {

        $response = array( 'success' => false );

        $user_data = array();
        $user_id = intval( $request['id'] );

        if( array_key_exists( 'username', $request ) ) {
            $user_data['username'] = $request['username'];
        }
        if( array_key_exists( 'password', $request ) ) {
            $user_data['password'] = $request['password'];
        }
        if( array_key_exists( 'email', $request ) ) {
            $user_data['email'] = $request['email'];
        }
        if( array_key_exists( 'first_name', $request ) ) {
            $user_data['first_name'] = $request['first_name'];
        }
        if( array_key_exists( 'last_name', $request ) ) {
            $user_data['last_name'] = $request['last_name'];
        }
        if( array_key_exists( 'avatar', $request ) ) {
            $user_data['picture'] = $request['avatar'];
        }

        if( array_key_exists( 'realtor', $request ) ) {

            // there's only one 'admin' so a
            // user is either 'realtor' or 'client'

            if( $request['realtor'] === true ) {

                if( ! $this->ion_auth->in_group( '3', $user_id ) ) {

                    $this->ion_auth->add_to_group( '3', $user_id );
                    $this->ion_auth->remove_from_group( '2', $user_id );
                }

            } else {

                if( ! $this->ion_auth->in_group( '2', $user_id ) ) {

                    $this->ion_auth->add_to_group( '2', $user_id );
                    $this->ion_auth->remove_from_group( '3', $user_id );
                }
            }
        }

        if( ! empty( $user_data ) ) {
            if( ! $this->ion_auth->update( $user_id, $user_data ) ) {
                $response['message'] = $this->ion_auth->errors();
                return $response;
            }
        }

        $response['success'] = true;
        
        return $response;
    }

    public function upload_photo_for_current_user( $request ) {

        $path = $request['full_path'];
        $file_content = file_get_contents( $path );
        $type = $request['file_type'];
        $base64 = 'data:' . $type . ';base64,' . base64_encode($file_content);

        $data['base64_image'] = $base64;

        $user = $this->ion_auth->user()->row();

        $response = $this->site_user_service->update_site_user([
            'id' => $user->id,
            'avatar' => $base64,
        ]);

        if( $response['success'] ) {
            $response['avatar'] = $base64;
        }

        return $response;
    }

    //////////////////////////////////////////////////////////
    //
    // RETRIEVE
    //
    //////////////////////////////////////////////////////////

    public function retrieve_user( $request ) {

        $response = array( 'success' => false );

        $user_id = $request['id'];

        $user_groups = $this->ion_auth->get_users_groups( $user_id )->result();

        if( empty( $user_groups ) ) {

            $response['message'] = 'invalid user account';
            return $response;
        }

        // Get only group names as array
        $user_groups = array_map( function( $el ) { return $el->name; }, $user_groups );

        $user = $this->ion_auth->user( $user_id )->row();

        $this->config->load('arm', TRUE);
        $avatar = $this->config->item('default_avatar', 'arm');

        $response['success'] = true;
        $response['user'] = array(
            'id' => $user->user_id,
            'username' => $user->username,
            'email' => $user->email,
            'groups' => $user_groups,
            'is_admin' => $this->ion_auth->is_admin( $user_id ),
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'social_login' => $user->social_login,
            'picture' => empty( $user->picture ) ? $avatar : $user->picture,
        );

        return $response;
    }

    public function retrieve_user_list( $request ) {

        $response = array( 'success' => false );

        // 2 -> 'client'
        $user_list = $this->ion_auth->users( '2' )->result();
        $user_list = array_map( function( $el ) {
            return array(
                'id' => $el->user_id,
                'username' => $el->username,
                'email' => $el->email,
                'active' => $el->active,
                'realtor' => false
            );
        }, $user_list );


        // 3 -> 'realtor'
        $realtors_user_list = $this->ion_auth->users( '3' )->result();
        $realtors_user_list = array_map( function( $el ) {
            return array(
                'id' => $el->user_id,
                'username' => $el->username,
                'email' => $el->email,
                'active' => $el->active,
                'realtor' => true
            );
        }, $realtors_user_list );

        $user_list = array_merge( $user_list, $realtors_user_list );

        $user_list = array_filter( $user_list, function( $el ) {
            return $el['active'];
        } );
        // we have to do this here because array_filter() PRESERVES KEYS
        // which may result in $user_list being converted to JSON OBJECT {} instead of
        // JSON ARRAY [] when sent to API client.
        $user_list = array_values( $user_list );

        $response['success'] = true;
        $response['users'] = $user_list;
        
        return $response;
    }

    //////////////////////////////////////////////////////////
    //
    // DELETE
    //
    //////////////////////////////////////////////////////////

    public function deactivate_user( $request ) {

        $response = array( 'success' => false );

        $user_id = $request['id'];

        if( ! $this->ion_auth->update( $user_id, [ 'active' => '0' ] ) ) {
            $response['message'] = $this->ion_auth->errors();
            return $response;
        }

        $response['success'] = true;
        
        return $response;
    }

    //////////////////////////////////////////////////////////
    //
    // OAUTH PROVIDERS
    //
    //////////////////////////////////////////////////////////

    public function register_oauth_account( $request ) {

        $this->config->load('arm', TRUE);
        $request['send_email'] = false;
        $request['password'] = $this->config->item('social_password', 'arm');

        $response = $this->register_site_client( $request );
        // $response['request'] = $request;

        if( ! $response['success'] ) {
            return $response;
        }

        $this->ion_auth->update(
            $response['user_data']['id'],
            [
                'social_login' => 1,
                'social_provider' => $request['provider'],
            ]
        );

        return $response;
    }

    //////////////////////////////////////////////////////////
    //
    // QUERY
    //
    //////////////////////////////////////////////////////////

    public function exists( $identity ) {

        return $this->ion_auth->identity_check( $identity );
    }

    public function is_client( $id = null ) {

        if( $id == null ) {
            $id = $this->ion_auth->user()->row()->user_id;
        }

        return $this->ion_auth->in_group( '2', $id );
    }

    public function is_realtor( $id = null ) {

        if( $id == null ) {
            $id = $this->ion_auth->user()->row()->user_id;
        }

        return $this->ion_auth->in_group( '3', $id );
    }
}
