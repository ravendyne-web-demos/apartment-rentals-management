<?php
/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */
defined('BASEPATH') OR exit('No direct script access allowed');

class Site_apartment_service {

    // protected $CI;

    public function __construct() {
    }

    // Enables the use of CI super-global without having to define an extra variable.
    public function __get($var) {
        return get_instance()->$var;
    }

    private function get_data_from_request( $request ) {

        $data = array();

        if( array_key_exists( 'name', $request ) ) {
            $data['name'] = $request['name'];
        }

        if( array_key_exists( 'description', $request ) ) {
            $data['description'] = $request['description'];
        }

        if( array_key_exists( 'floor_area_size', $request ) ) {
            $data['floor_area_size'] = intval( $request['floor_area_size'] );
        }

        if( array_key_exists( 'price_per_month', $request ) ) {
            $data['price_per_month'] = floatval( $request['price_per_month'] );
        }

        if( array_key_exists( 'number_of_rooms', $request ) ) {
            $data['number_of_rooms'] = intval( $request['number_of_rooms'] );
        }

        if( array_key_exists( 'geolocation_lat', $request ) ) {
            $data['geolocation_lat'] = floatval( $request['geolocation_lat'] );
        }

        if( array_key_exists( 'geolocation_long', $request ) ) {
            $data['geolocation_long'] = floatval( $request['geolocation_long'] );
        }

        if( array_key_exists( 'address', $request ) ) {
            $data['address'] = $request['address'];
        }

        if( array_key_exists( 'associated_realtor', $request ) ) {
            $data['associated_realtor'] = intval( $request['associated_realtor'] );
        }

        if( array_key_exists( 'is_rented', $request ) ) {
            $data['is_rented'] = boolval( $request['is_rented'] );
        }

        return $data;
    }

    public function create_apartment( $request ) {

        $response = array( 'success' => false );

        $date_added = date( 'Y-m-d' ); // YYYY-MM-DD

        $data = $this->get_data_from_request( $request );

        $user_id = null;

        if( $this->ion_auth->is_admin() && array_key_exists( 'realtor_id', $request ) ) {

            // admin gets to create for anyone
            $user_id = intval( $request['realtor_id'] );

        } else {

            $user = $this->ion_auth->user()->row();
            $user_id = $user->user_id;
        }

        $associated_realtor = $user_id;

        $data['date_added'] = $date_added . ' ' . date( 'H:i:s' );
        $data['associated_realtor'] = $associated_realtor;
        $data['is_rented'] = false;

        $insert_success = $this->db->insert( 'apartments', $data );

        $response['success'] = $insert_success;
        $response['data'] = $data;
        if( $insert_success ) {
            $response['data']['id'] = $this->db->insert_id();
        }

		return $response;
    }

    public function retrieve_apartment( $request ) {

        $user = $this->ion_auth->user()->row();

        $filter = array();

        if( array_key_exists( 'id', $request ) ) {

            $filter['id'] = $request['id'];

        } else if( array_key_exists( 'filter', $request ) ) {

            $request_filter = $request['filter'];

            if( array_key_exists( 'size_from', $request_filter ) ) {
                $filter['floor_area_size >='] = $request_filter['size_from'];
            }
            if( array_key_exists( 'size_to', $request_filter ) ) {
                $filter['floor_area_size <='] = $request_filter['size_to'];
            }
            if( array_key_exists( 'price_from', $request_filter ) ) {
                $filter['price_per_month >='] = $request_filter['price_from'];
            }
            if( array_key_exists( 'price_to', $request_filter ) ) {
                $filter['price_per_month <='] = $request_filter['price_to'];
            }
            if( array_key_exists( 'rooms_from', $request_filter ) ) {
                $filter['number_of_rooms >='] = $request_filter['rooms_from'];
            }
            if( array_key_exists( 'rooms_to', $request_filter ) ) {
                $filter['number_of_rooms <='] = $request_filter['rooms_to'];
            }
            if( array_key_exists( 'realtor_id', $request_filter ) ) {
                $filter['associated_realtor'] = intval( $request_filter['realtor_id'] );
            }
            if( array_key_exists( 'is_rented', $request_filter ) ) {
                $filter['is_rented'] = boolval( $request_filter['is_rented'] );
            }
        }

        $this->db->select( '*' );
        if( ! empty( $filter ) ) {
            $this->db->where( $filter );
        }

        // Filter out already rented apartments for clients
        // '2' => client group
        if( $this->ion_auth->in_group('2') ) {
            $this->db->where( [ 'is_rented' => 0 ] );
        }

        $query = $this->db->get( 'apartments' );

        $apartments = array();
        foreach( $query->result() as $row )
        {
            $apartments[] = $row;
        }

        $response = array( 'success' => count( $apartments ) > 0 );
        $response['apartments'] = $apartments;
        $response['filter'] = $filter;

		return $response;
    }

    public function update_apartment( $request ) {

        $response = array( 'success' => false );

        $id = intval( $request['id'] );
        $data = array();
        $datetime_value = '';

        $data = $this->get_data_from_request( $request );

        if( array_key_exists( 'realtor_id', $request ) ) {
            $this->db->where( 'associated_realtor', $request['realtor_id'] );
        }

        $this->db->where( 'id', $id );
        $this->db->update( 'apartments', $data );

        $response['success'] = $this->db->affected_rows() > 0;
        $response['data'] = $data;

		return $response;
    }

    public function delete_apartment( $request ) {

        $response = array( 'success' => false );

        $id = intval( $request['id'] );

        // only admin gets to delete for anyone
        if( ! $this->ion_auth->is_admin() ) {

            $user = $this->ion_auth->user()->row();

            $this->db->where( 'associated_realtor', $user->user_id );
        }

        $this->db->where( 'id', $id );
        $this->db->delete( 'apartments' );

        $response['success'] = $this->db->affected_rows() > 0;
        $response['request'] = $request;

		return $response;
    }

    public function delete_apartments_for_realtor( $request ) {

        $response = array( 'success' => false );

        $realtor_id = intval( $request['id'] );

        // only admin gets to delete for anyone
        if( ! $this->ion_auth->is_admin() ) {

            $user = $this->ion_auth->user()->row();
            $realtor_id = $user->user_id;
        }

        $this->db->reset_query();
        $this->db->where( 'associated_realtor', $realtor_id );
        $this->db->delete( 'apartments' );

        $response['success'] = $this->db->affected_rows() > 0;
        $response['request'] = $request;

		return $response;
    }
}
