<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['protocol'] = 'smtp';
$config['smtp_host'] = 'localhost';
$config['smtp_port'] = '25';
// $config['smtp_user'] = 'usr';
// $config['smtp_pass'] = 'pwd';

// Since Python smptd module does not support EHLO command,
// we have to make CI Email library use HELO instead
// In order to do that we need to:
//  - disable SMTP authentication
//  - use one of 7bit charsets (us-ascii or one of iso-2022-...)
// these settings do just that
$config['smtp_user'] = '';
$config['smtp_pass'] = '';
$config['charset'] = 'us-ascii'; // default: $config['charset']
// Python smtpd expects <CR><LF>
$config['newline'] = "\r\n";
