<?php
/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */
defined('BASEPATH') OR exit('No direct script access allowed');

$config['social_password'] = '@password123$';
$config['oauth2_redirect_uri']['github'] = 'http://localhost:8080/api/oauth_callback_github.php';
$config['oauth2_redirect_uri']['google'] = 'http://localhost:8080/api/oauth_callback_google.php';
$config['oauth2_redirect_uri']['instagram'] = 'http://localhost:8080/api/oauth_callback_instagram.php';

// $config['default_avatar'] = './img/avatar.png';
$config['default_avatar'] = './img/avatar-2.jpg';
