<?php

ob_start();
include('index.php');
ob_end_clean();
$CI =& get_instance();
$CI->load->driver('session');
// var_dump($_SESSION);


// Check given state against previously stored one to mitigate CSRF attack
if( empty($_GET['state']) || ($_GET['state'] !== $CI->session->userdata('oauth2_google_state')) ) {

    $CI->session->unset_userdata('oauth2_google_state');
    echo 'Invalid state';
    echo print_r($_SESSION, true);
    return;
}

$url = '/api/index.php?/auth/oauthcallback_google';

if( isset($_GET['code']) ) {
    $url .= '/code' . '/' . urlencode( $_GET['code'] );
}
if( isset($_GET['state']) ) {
    $url .= '/state' . '/' . $_GET['state'];
}

header('Location: ' . $url);
