#!/usr/bin/env bash

sudo apt-get update
sudo apt-get install php-fpm php-mysql php-mbstring php-mcrypt php-xml -y
sudo apt-get install nginx -y

MYSQL_PASS="root"
echo "mysql-server mysql-server/root_password password $MYSQL_PASS" | sudo debconf-set-selections
echo "mysql-server mysql-server/root_password_again password $MYSQL_PASS" | sudo debconf-set-selections
echo "phpmyadmin phpmyadmin/dbconfig-install boolean true" | sudo debconf-set-selections
echo "phpmyadmin phpmyadmin/app-password-confirm password $MYSQL_PASS" | sudo debconf-set-selections
echo "phpmyadmin phpmyadmin/mysql/admin-pass password $MYSQL_PASS" | sudo debconf-set-selections
echo "phpmyadmin phpmyadmin/mysql/app-pass password $MYSQL_PASS" | sudo debconf-set-selections
echo "phpmyadmin phpmyadmin/reconfigure-webserver multiselect none" | sudo debconf-set-selections
sudo apt-get -y install mysql-server phpmyadmin

# make Mysql accessible from the host machine
sudo sed -r -i.bak "s/(bind-address\s*=\s*127.0.0.1)/# \1/" /etc/mysql/mysql.conf.d/mysqld.cnf

sudo systemctl restart mysql

mysql -u root -p$MYSQL_PASS << EOF
CREATE USER 'dev'@'localhost' IDENTIFIED BY 'devpass';
GRANT ALL PRIVILEGES ON *.* TO 'dev'@'localhost' WITH GRANT OPTION;
CREATE USER 'dev'@'%' IDENTIFIED BY 'devpass';
GRANT ALL PRIVILEGES ON *.* TO 'dev'@'%' WITH GRANT OPTION;
EOF

sudo phpenmod mcrypt
sudo systemctl restart php7.0-fpm
sudo ln -s /usr/share/phpmyadmin /var/www/html/phpmyadmin
sudo chown -R www-data.www-data /var/www/html/phpmyadmin/

cat > /etc/nginx/snippets/phpmyadmin.conf << "EOF"
location /phpmyadmin {
    root /usr/share/;
    index index.php index.html index.htm;
    location ~ ^/phpmyadmin/(.+\.php)$ {
        try_files $uri =404;
        root /usr/share/;
        fastcgi_pass unix:/run/php/php7.0-fpm.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include /etc/nginx/fastcgi_params;
    }

    location ~* ^/phpmyadmin/(.+\.(jpg|jpeg|gif|css|png|js|ico|html|xml|txt))$ {
        root /usr/share/;
    }
}
EOF

sudo -u root sh -c "cat > /etc/nginx/sites-available/default" << "EOF"
#
# Default server configuration
#
server {
    listen 80 default_server;
    listen [::]:80 default_server;

    include snippets/phpmyadmin.conf;

    root /var/www/html;

    index index.php index.html index.htm index.nginx-debian.html;

    server_name _;

    location / {
        try_files $uri $uri/ =404;
    }

    #
    # REST API calls
    # convert uri segments into 'apireq' GET parameter
    #
    location ~ ^/api/v0(.*)/?$ {
        try_files $uri $uri/ /api/index.php?apireq=$1&$args;
    }

    location ~ \.php$ {
        include snippets/fastcgi-php.conf;

        # With php7.0-cgi alone:
        #fastcgi_pass 127.0.0.1:9000;
        # With php7.0-fpm:
        fastcgi_pass unix:/run/php/php7.0-fpm.sock;
    }

    #
    # deny access to .htaccess files, if Apache's document root
    # concurs with nginx's one
    #
    #location ~ /\.ht {
    #    deny all;
    #}

    #
    # debug rewrites
    #
    #rewrite_log on;
    #error_log /var/log/nginx/localhost.error_log notice;
}
EOF

#sudo nginx -t
sudo systemctl reload nginx
