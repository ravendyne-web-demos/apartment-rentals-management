## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later

import unittest

# import all test modules
import tests.app_login
import tests.client_apartments_filter
import tests.realtor_apartments_crud
import tests.admin_apartments_crud
# import tests.admin_users_crud


# initialize the test suite
loader = unittest.TestLoader()
suite  = unittest.TestSuite()

# add tests from modules to the test suite
suite.addTests( loader.loadTestsFromModule( tests.app_login ) )
suite.addTests( loader.loadTestsFromModule( tests.client_apartments_filter ) )
suite.addTests( loader.loadTestsFromModule( tests.realtor_apartments_crud ) )
suite.addTests( loader.loadTestsFromModule( tests.admin_apartments_crud ) )
# suite.addTests( loader.loadTestsFromModule( tests.admin_users_crud ) )

# initialize a runner and run our test suite with it
runner = unittest.TextTestRunner( verbosity = 3 )
result = runner.run( suite )
