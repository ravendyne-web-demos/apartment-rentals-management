
SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";



USE `rentals_site`;


TRUNCATE TABLE `apartments`;


-- REALTOR 3 ----------
-- APARTMENT 300 ----------

INSERT INTO `apartments` (
    `id`, `name`, `description`,
    `floor_area_size`, `price_per_month`, `number_of_rooms`,
    `geolocation_lat`, `geolocation_long`, 
    `date_added`, 
    `associated_realtor`, 
    `is_rented`, 
    `address`
) VALUES (
    300, 'apartment A', 'apartment A description', 
    '300.000', '30.00', 2, 
    '43.6692', '-79.3939',
    '2019-08-06', 
    3, 
    0, 
    'apartment A address'
);

-- APARTMENT 301 ----------

INSERT INTO `apartments` (
    `id`, `name`, `description`,
    `floor_area_size`, `price_per_month`, `number_of_rooms`,
    `geolocation_lat`, `geolocation_long`, 
    `date_added`, 
    `associated_realtor`, 
    `is_rented`, 
    `address`
) VALUES (
    301, 'apartment B', 'apartment B description', 
    '400.000', '40.00', 3, 
    '43.6680', '-79.4112',
    '2019-08-06', 
    3, 
    1, 
    'apartment B address'
);

-- REALTOR 5 ----------
-- APARTMENT 500 ----------

INSERT INTO `apartments` (
    `id`, `name`, `description`,
    `floor_area_size`, `price_per_month`, `number_of_rooms`,
    `geolocation_lat`, `geolocation_long`, 
    `date_added`, 
    `associated_realtor`, 
    `is_rented`, 
    `address`
) VALUES (
    500, 'apartment C', 'apartment C description', 
    '500.000', '50.00', 4, 
    '43.6615', '-79.3982',
    '2019-08-06', 
    5, 
    0, 
    'apartment C address'
);

-- APARTMENT 501 ----------

INSERT INTO `apartments` (
    `id`, `name`, `description`,
    `floor_area_size`, `price_per_month`, `number_of_rooms`,
    `geolocation_lat`, `geolocation_long`, 
    `date_added`, 
    `associated_realtor`, 
    `is_rented`, 
    `address`
) VALUES (
    501, 'apartment D', 'apartment D description', 
    '600.000', '60.00', 5, 
    '43.6547', '-79.3920',
    '2019-08-06', 
    5, 
    1, 
    'apartment D address'
);




SET FOREIGN_KEY_CHECKS=1;

COMMIT;
