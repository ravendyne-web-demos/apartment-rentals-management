## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later

from selenium.webdriver.common.by import By

class AppFormLocators(object):
    CLIENT_APARTMENT_TABLE = (By.ID, 'client-apartments-table')
    CLIENT_APARTMENT_TABLE_XPATH = "//table[@id='client-apartments-table']"

    CLIENT_APARTMENT_FILTER_FORM = (By.ID, 'client-filter-form')
    CLIENT_APARTMENT_FILTER_FORM_XPATH = "//form[@id='client-filter-form']"

    ADMIN_USER_FORM = (By.ID, 'admin-user-form')
    ADMIN_USER_FORM_XPATH = "//form[@id='admin-user-form']"
    ADMIN_USER_TABLE = (By.ID, 'admin-users-table')
    ADMIN_USER_TABLE_XPATH = "//table[@id='admin-users-table']"

    ADMIN_DASHBOARD = (By.ID, 'admin-dashboard')
    ADMIN_DASHBOARD_XPATH = "//form[@id='admin-dashboard']"
    ADMIN_DASHBOARD_USERS = (By.ID, 'admin-dashboard-users')
    ADMIN_DASHBOARD_USERS_XPATH = "//form[@id='admin-dashboard-users']"
    ADMIN_DASHBOARD_APARTMENTS = (By.ID, 'admin-dashboard-apartments')
    ADMIN_DASHBOARD_APARTMENTS_XPATH = "//form[@id='admin-dashboard-apartments']"

class ToolbarLocators(object):
    LOGOUT_BUTTON = (By.NAME, 'logout-button')

class LoginPageLocators(object):
    IDENTITY_FIELD = (By.ID, 'login-identity')
    PASSWORD_FIELD = (By.ID, 'login-password')
    LOGIN_BUTTON = (By.NAME, 'login-button')

class UsersTableLocators(object):
    DELETE_ROW_BUTTON = (By.NAME, 'user-delete-button')
    DELETE_ROW_BUTTON_XPATH = "//button[@name='user-delete-button']"


class ApartmentsFormLocators(object):
    APARTMENT_FORM = (By.ID, 'admin-apartment-form')
    APARTMENT_FORM_XPATH = "//form[@id='admin-apartment-form']"

    NAME_FIELD = (By.ID, 'apartment-name')
    NAME_FIELD_XPATH = "//input[@id='apartment-name']"
    DESCRIPTION_FIELD = (By.ID, 'apartment-description')
    DESCRIPTION_FIELD_XPATH = "//input[@id='apartment-description']"
    SIZE_FIELD = (By.ID, 'apartment-size')
    SIZE_FIELD_XPATH = "//input[@id='apartment-size']"
    PRICE_FIELD = (By.ID, 'apartment-price')
    PRICE_FIELD_XPATH = "//input[@id='apartment-price']"
    ROOMS_FIELD = (By.ID, 'apartment-rooms')
    ROOMS_FIELD_XPATH = "//input[@id='apartment-rooms']"
    ADDRESS_FIELD = (By.ID, 'apartment-address')
    ADDRESS_FIELD_XPATH = "//input[@id='apartment-address']"
    LAT_FIELD = (By.ID, 'apartment-lat')
    LAT_FIELD_XPATH = "//input[@id='apartment-lat']"
    LON_FIELD = (By.ID, 'apartment-lon')
    LON_FIELD_XPATH = "//input[@id='apartment-lon']"
    RENTED_FIELD = (By.ID, 'apartment-rented')
    RENTED_FIELD_XPATH = "//input[@id='apartment-rented']"
    REALTOR_FIELD = (By.ID, 'apartment-realtor')
    REALTOR_FIELD_XPATH = "//input[@id='apartment-realtor']"

    ADD_BUTTON = (By.NAME, 'apartment-add-button')
    ADD_BUTTON_XPATH = "//button[@name='apartment-add-button']"
    UPDATE_BUTTON = (By.NAME, 'apartment-update-button')
    UPDATE_BUTTON_XPATH = "//button[@name='apartment-update-button']"
    RESET_BUTTON = (By.NAME, 'apartment-reset-button')
    RESET_BUTTON_XPATH = "//button[@name='apartment-reset-button']"

class ApartmentsTableLocators(object):
    DELETE_ROW_BUTTON = (By.ID, 'apartment-delete-button')
    DELETE_ROW_BUTTON_XPATH = "//button[@id='apartment-delete-button']"
    APARTMENT_DETAIL_BUTTON = (By.ID, 'apartment-detail-button')
    APARTMENT_DETAIL_BUTTON_XPATH = "//button[@id='apartment-detail-button']"
    APARTMENT_RENT_BUTTON = (By.ID, 'apartment-rent-button')
    APARTMENT_RENT_BUTTON_XPATH = "//button[@id='apartment-rent-button']"


class ApartmentsFilterFormLocators(object):
    SIZE_FROM_FIELD = (By.ID, 'filter-size-from')
    SIZE_TO_FIELD = (By.ID, 'filter-size-to')
    PRICE_FROM_FIELD = (By.ID, 'filter-price-from')
    PRICE_TO_FIELD = (By.ID, 'filter-price-to')
    ROOMS_FROM_FIELD = (By.ID, 'filter-rooms-from')
    ROOMS_TO_FIELD = (By.ID, 'filter-rooms-to')

    FILTER_TOGGLE_BUTTON = (By.ID, 'filter-toggle-button')
    FILTER_TOGGLE_BUTTON_XPATH = "//button[@id='filter-toggle-button']"
    FILTER_BUTTON = (By.NAME, 'apartment-filter-button')
    FILTER_BUTTON_XPATH = "//button[@name='apartment-filter-button']"
    RESET_BUTTON = (By.NAME, 'filter-reset-button')
    RESET_BUTTON_XPATH = "//button[@name='filter-reset-button']"
