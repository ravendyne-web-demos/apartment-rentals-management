## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later

from lib_base import LibBaseClass, DataTable
from locators import AppFormLocators
from locators import ApartmentsFormLocators, ApartmentsFilterFormLocators
from locators import ApartmentsTableLocators

from selenium.webdriver.support import expected_conditions as expected
from selenium.webdriver.support.ui import Select

#
# Base class(es) for other classes
#

class ApartmentsForm(LibBaseClass):

    def locator(self):
        return ApartmentsFormLocators.APARTMENT_FORM

    def get_form(self):
        return self.driver.find_element( *self.locator() )

    def visible(self):
        elem_size = self.get_form().find_element( *ApartmentsFormLocators.SIZE_FIELD )
        elem_price = self.get_form().find_element( *ApartmentsFormLocators.PRICE_FIELD )
        elem_rooms = self.get_form().find_element( *ApartmentsFormLocators.ROOMS_FIELD )

        def cond( driver ):
            return \
                expected.visibility_of( elem_size )( driver ) and \
                expected.visibility_of( elem_price )( driver ) and \
                expected.visibility_of( elem_rooms )( driver )
        return cond

    def get_field_values(self):
        elem = self.get_form().find_element( *ApartmentsFormLocators.NAME_FIELD )
        name = elem.get_attribute( 'value' )

        elem = self.get_form().find_element( *ApartmentsFormLocators.DESCRIPTION_FIELD )
        description = elem.get_attribute( 'value' )

        elem = self.get_form().find_element( *ApartmentsFormLocators.SIZE_FIELD )
        size = elem.get_attribute( 'value' )

        elem = self.get_form().find_element( *ApartmentsFormLocators.PRICE_FIELD )
        price = elem.get_attribute( 'value' )

        elem = self.get_form().find_element( *ApartmentsFormLocators.ROOMS_FIELD )
        rooms = elem.get_attribute( 'value' )

        elem = self.get_form().find_element( *ApartmentsFormLocators.ADDRESS_FIELD )
        address = elem.get_attribute( 'value' )

        elem = self.get_form().find_element( *ApartmentsFormLocators.LAT_FIELD )
        lat = elem.get_attribute( 'value' )

        elem = self.get_form().find_element( *ApartmentsFormLocators.LON_FIELD )
        lon = elem.get_attribute( 'value' )

        elem = self.get_form().find_element( *ApartmentsFormLocators.REALTOR_FIELD )
        realtor = elem.get_attribute( 'value' )

        elem = self.get_form().find_element( *ApartmentsFormLocators.RENTED_FIELD )
        rented = elem.get_attribute( 'value' )

        return (name, description, size, price, rooms, address, lat, lon, realtor, rented)


    def fill_form(self, name, description, size, price, rooms, address, lat, lon, realtor, rented):
        if name:
            elem = self.get_form().find_element( *ApartmentsFormLocators.NAME_FIELD )
            elem.clear()
            elem.send_keys( name )

        if description:
            elem = self.get_form().find_element( *ApartmentsFormLocators.DESCRIPTION_FIELD )
            elem.clear()
            elem.send_keys( description )

        if size:
            elem = self.get_form().find_element( *ApartmentsFormLocators.SIZE_FIELD )
            elem.clear()
            elem.send_keys( size )

        if price:
            elem = self.get_form().find_element( *ApartmentsFormLocators.PRICE_FIELD )
            elem.clear()
            elem.send_keys( price )

        if rooms:
            elem = self.get_form().find_element( *ApartmentsFormLocators.ROOMS_FIELD )
            elem.clear()
            elem.send_keys( rooms )

        if address:
            elem = self.get_form().find_element( *ApartmentsFormLocators.ADDRESS_FIELD )
            elem.clear()
            elem.send_keys( address )

        if lat:
            elem = self.get_form().find_element( *ApartmentsFormLocators.LAT_FIELD )
            self.driver.execute_script("document.getElementById('" + ApartmentsFormLocators.LAT_FIELD[1] + "').setAttribute('value', '" + str(lat) + "')")

        if lon:
            elem = self.get_form().find_element( *ApartmentsFormLocators.LON_FIELD )
            self.driver.execute_script("document.getElementById('" + ApartmentsFormLocators.LON_FIELD[1] + "').setAttribute('value', '" + str(lon) + "')")

        if realtor:
            elem = self.get_form().find_element( *ApartmentsFormLocators.REALTOR_FIELD )
            select = Select( elem )
            select.select_by_value( realtor )

        if rented:
            elem = self.get_form().find_element( *ApartmentsFormLocators.RENTED_FIELD )
            select = Select( elem )
            select.select_by_value( rented )

    def do_add(self):
        elem = self.get_form().find_element( *ApartmentsFormLocators.ADD_BUTTON )
        elem.click()

    def do_update(self):
        elem = self.get_form().find_element( *ApartmentsFormLocators.UPDATE_BUTTON )
        elem.click()

    def do_reset(self):
        elem = self.get_form().find_element( *ApartmentsFormLocators.RESET_BUTTON )
        elem.click()

class ApartmentsFilterForm(LibBaseClass):

    def locator(self):
        return AppFormLocators.CLIENT_APARTMENT_FILTER_FORM

    def get_form(self):
        return self.driver.find_element( *self.locator() )

    def visible(self):
        elem_size_from = self.get_form().find_element( *ApartmentsFilterFormLocators.SIZE_FROM_FIELD )
        elem_size_to = self.get_form().find_element( *ApartmentsFilterFormLocators.SIZE_TO_FIELD )
        elem_price_from = self.get_form().find_element( *ApartmentsFilterFormLocators.PRICE_FROM_FIELD )
        elem_price_to = self.get_form().find_element( *ApartmentsFilterFormLocators.PRICE_TO_FIELD )

        def cond( driver ):
            return \
                expected.visibility_of( elem_size_from )( driver ) and \
                expected.visibility_of( elem_size_to )( driver ) and \
                expected.visibility_of( elem_price_from )( driver ) and \
                expected.visibility_of( elem_price_to )( driver )
        return cond
    
    def toggle_filter_form(self):
        driver = self.driver

        elem = driver.find_element( *ApartmentsFilterFormLocators.FILTER_TOGGLE_BUTTON )
        elem.click()

    def get_field_values(self):
        elem = self.get_form().find_element( *ApartmentsFilterFormLocators.SIZE_FROM_FIELD )
        size_from = elem.get_attribute( 'value' )

        elem = self.get_form().find_element( *ApartmentsFilterFormLocators.SIZE_TO_FIELD )
        size_to = elem.get_attribute( 'value' )

        elem = self.get_form().find_element( *ApartmentsFilterFormLocators.PRICE_FROM_FIELD )
        price_from = elem.get_attribute( 'value' )

        elem = self.get_form().find_element( *ApartmentsFilterFormLocators.PRICE_TO_FIELD )
        price_to = elem.get_attribute( 'value' )

        elem = self.get_form().find_element( *ApartmentsFilterFormLocators.ROOMS_FROM_FIELD )
        rooms_from = elem.get_attribute( 'value' )

        elem = self.get_form().find_element( *ApartmentsFilterFormLocators.ROOMS_TO_FIELD )
        rooms_to = elem.get_attribute( 'value' )

        return (size_from, size_to, price_from, price_to, rooms_from, rooms_to)

    def fill_form(self, size_from, size_to, price_from, price_to, rooms_from, rooms_to):
        if size_from:
            elem = self.get_form().find_element( *ApartmentsFilterFormLocators.SIZE_FROM_FIELD )
            elem.clear()
            elem.send_keys( size_from )

        if size_to:
            elem = self.get_form().find_element( *ApartmentsFilterFormLocators.SIZE_TO_FIELD )
            elem.clear()
            elem.send_keys( size_to )

        if price_from:
            elem = self.get_form().find_element( *ApartmentsFilterFormLocators.PRICE_FROM_FIELD )
            elem.clear()
            elem.send_keys( price_from )

        if price_to:
            elem = self.get_form().find_element( *ApartmentsFilterFormLocators.PRICE_TO_FIELD )
            elem.clear()
            elem.send_keys( price_to )

        if rooms_from:
            elem = self.get_form().find_element( *ApartmentsFilterFormLocators.ROOMS_FROM_FIELD )
            elem.clear()
            elem.send_keys( rooms_from )

        if rooms_to:
            elem = self.get_form().find_element( *ApartmentsFilterFormLocators.ROOMS_TO_FIELD )
            elem.clear()
            elem.send_keys( rooms_to )

    def do_filter(self):
        elem = self.get_form().find_element( *ApartmentsFilterFormLocators.FILTER_BUTTON )
        elem.click()

    def do_reset(self):
        elem = self.get_form().find_element( *ApartmentsFilterFormLocators.RESET_BUTTON )
        elem.click()


class ApartmentsTable(DataTable):

    def locator(self):
        return AppFormLocators.CLIENT_APARTMENT_TABLE

    def find_row_delete_button(self, elem):
        return elem.find_element( *ApartmentsTableLocators.DELETE_ROW_BUTTON )

    def find_row_detail_button(self, elem):
        return elem.find_element( *ApartmentsTableLocators.APARTMENT_DETAIL_BUTTON )

    def show_add_form(self):
        elem = self.driver.find_element( *ApartmentsFormLocators.ADD_BUTTON )
        elem.click()
