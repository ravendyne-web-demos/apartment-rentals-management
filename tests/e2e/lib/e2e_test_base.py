## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later

import e2e_config as cfg

import unittest
import requests
import json

import mysql.connector

from helper_tools import RunSQLFile


class E2eTestBase(unittest.TestCase):

    def setUp(self):
        self.db_rebuild_database()

    def do_login_admin(self):
        self.do_login( 'admin@arm.com', 'password' )

    def do_login_client(self):
        self.do_login( 'client@arm.com', 'password' )

    def do_login_realtor(self):
        self.do_login( 'realtor@arm.com', 'password' )

    def do_login(self, identity, password):
        pass

    def do_logout(self):
        pass

    def db_rebuild_database(self):

        conn = mysql.connector.connect(
            database = cfg.APP_DB_DATABASE,
            host = cfg.APP_DB_HOST,
            user = cfg.APP_DB_USER,
            passwd = cfg.APP_DB_PASS
        )

        RunSQLFile( conn, cfg.APP_DB_CREATE_SQL_FILE )
        RunSQLFile( conn, cfg.APP_DB_ION_AUTH_SQL_FILE )
        RunSQLFile( conn, cfg.APP_DB_APP_TABLES_SQL_FILE )
        RunSQLFile( conn, cfg.APP_DB_USERS_SQL_FILE )

        conn.close()

    def db_load_users(self):

        conn = mysql.connector.connect(
            database = cfg.APP_DB_DATABASE,
            host = cfg.APP_DB_HOST,
            user = cfg.APP_DB_USER,
            passwd = cfg.APP_DB_PASS
        )

        RunSQLFile( conn, cfg.APP_DB_USERS_SQL_FILE )
        RunSQLFile( conn, cfg.APP_DB_TEST_USERS_SQL_FILE )

        conn.close()

    def db_load_apartments(self):

        conn = mysql.connector.connect(
            database = cfg.APP_DB_DATABASE,
            host = cfg.APP_DB_HOST,
            user = cfg.APP_DB_USER,
            passwd = cfg.APP_DB_PASS
        )

        RunSQLFile( conn, cfg.APP_DB_APARTMENTS_SQL_FILE )

        conn.close()

