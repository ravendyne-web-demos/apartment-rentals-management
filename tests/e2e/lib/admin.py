## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later

from lib_base import LibBaseClass, DataTable
from locators import AppFormLocators

from selenium.webdriver.support import expected_conditions as expected
from selenium.webdriver.support.ui import Select



#
# Classes for specific forms and tables
#

class AdminDashboard(LibBaseClass):

    def locator(self):
        return AppFormLocators.ADMIN_DASHBOARD

    def visible(self):
        def cond( driver ):
            return \
                expected.visibility_of_element_located( AppFormLocators.ADMIN_DASHBOARD_USERS )( driver ) and \
                expected.visibility_of_element_located( AppFormLocators.ADMIN_DASHBOARD_APARTMENTS )( driver )
        return cond

    def navigate_to_users(self):
        driver = self.driver

        elem = driver.find_element( *AppFormLocators.ADMIN_DASHBOARD_USERS )
        elem.click()

    def navigate_to_apartments(self):
        driver = self.driver

        elem = driver.find_element( *AppFormLocators.ADMIN_DASHBOARD_APARTMENTS )
        elem.click()


