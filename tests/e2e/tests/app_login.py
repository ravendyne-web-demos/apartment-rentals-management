## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later

import e2e_config as cfg

import unittest

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import time

from lib.login_page import LoginPage
from lib.toolbar import Toolbar
from lib.apartments import ApartmentsTable
from lib.admin import AdminDashboard

from lib.locators import LoginPageLocators

class AppLogin(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait( 5 ) # seconds

    def test_login_without_credentials(self):
        driver = self.driver
        wait = WebDriverWait( driver, 5 ) # seconds

        driver.get( cfg.APP_BASE_URL )

        login_page = LoginPage( driver )

        wait.until( login_page.visible() )

        login_page.do_login()
        # time.sleep( 3 ) #secs

        ## TODO assert error message visible

    def test_login_with_credentials(self):
        driver = self.driver
        wait = WebDriverWait( driver, 5 ) # seconds

        driver.get( cfg.APP_BASE_URL )

        login_page = LoginPage( driver )

        wait.until( login_page.visible() )

        login_page.fill_form( cfg.CLIENT_IDENTITY, cfg.CLIENT_PASSWORD )
        login_page.do_login()
        # time.sleep( 3 ) #secs

        toolbar = Toolbar( driver )
        wait.until( toolbar.visible() )

        toolbar.do_logout()
        # time.sleep( 3 ) #secs

        wait.until( login_page.visible() )

    def test_login_as_client(self):
        driver = self.driver
        wait = WebDriverWait( driver, 5 ) # seconds

        driver.get( cfg.APP_BASE_URL )

        login_page = LoginPage( driver )

        wait.until( login_page.visible() )

        login_page.fill_form( cfg.CLIENT_IDENTITY, cfg.CLIENT_PASSWORD )
        login_page.do_login()
        # time.sleep( 3 ) #secs

        apartments_table = ApartmentsTable( driver )
        wait.until( apartments_table.visible() )

        toolbar = Toolbar( driver )
        wait.until( toolbar.visible() )

        toolbar.do_logout()
        # time.sleep( 3 ) #secs

        wait.until( login_page.visible() )


    def test_login_as_realtor(self):
        driver = self.driver
        wait = WebDriverWait( driver, 5 ) # seconds

        driver.get( cfg.APP_BASE_URL )

        login_page = LoginPage( driver )

        wait.until( login_page.visible() )

        login_page.fill_form( cfg.REALTOR_IDENTITY, cfg.REALTOR_PASSWORD )
        login_page.do_login()
        # time.sleep( 3 ) #secs

        apartments_table = ApartmentsTable( driver )
        wait.until( apartments_table.visible() )

        toolbar = Toolbar( driver )
        wait.until( toolbar.visible() )

        toolbar.do_logout()
        # time.sleep( 3 ) #secs

        wait.until( login_page.visible() )


    def test_login_as_admin(self):
        driver = self.driver
        wait = WebDriverWait( driver, 5 ) # seconds

        driver.get( cfg.APP_BASE_URL )

        login_page = LoginPage( driver )

        wait.until( login_page.visible() )

        login_page.fill_form( cfg.ADMIN_IDENTITY, cfg.ADMIN_PASSWORD )
        login_page.do_login()
        # time.sleep( 3 ) #secs

        admin_dashboard = AdminDashboard( driver )
        wait.until( admin_dashboard.visible() )

        toolbar = Toolbar( driver )
        wait.until( toolbar.visible() )

        toolbar.do_logout()
        # time.sleep( 3 ) #secs

        wait.until( login_page.visible() )


    def tearDown(self):
        self.driver.close()

