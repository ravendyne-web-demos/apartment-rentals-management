## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later

import e2e_config as cfg

import unittest

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.alert import Alert

import time

from lib.app_helpers import DBResetApartments
from lib.app_helpers import AppHelper

from lib.toolbar import Toolbar
from lib.login_page import LoginPage
from lib.apartments import ApartmentsForm, ApartmentsTable
from lib.locators import AppFormLocators


class RealtorApartmentsCRUD(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox()


    def test_name_click_opens_form(self):
        driver = self.driver
        driver.implicitly_wait( 5 ) # seconds
        wait = WebDriverWait( driver, 5 ) # seconds
        DBResetApartments()

        app_helper = AppHelper( driver )
        app_helper.login( cfg.REALTOR_IDENTITY, cfg.REALTOR_PASSWORD )
        # time.sleep( 3 ) #secs

        apartments_table = ApartmentsTable( driver )
        wait.until( apartments_table.visible() )
        wait.until( apartments_table.expected_to_contain_row_with_text( 'apartment A' ) )

        elem = apartments_table.find_row_with_text( 'apartment A' )
        btn = apartments_table.find_row_detail_button( elem )
        btn.click()

        apartments_form = ApartmentsForm( driver )
        wait.until( apartments_form.visible() )

        form_values = apartments_form.get_field_values()
        self.assertTrue( form_values[0] )
        self.assertTrue( form_values[1] )
        self.assertTrue( form_values[2] )

        app_helper.logout()


    def _test_add(self):
        driver = self.driver
        driver.implicitly_wait( 5 ) # seconds
        wait = WebDriverWait( driver, 5 ) # seconds
        DBResetApartments()

        app_helper = AppHelper( driver )
        apartments_form = ApartmentsForm( driver )
        apartments_table = ApartmentsTable( driver )

        app_helper.login( cfg.REALTOR_IDENTITY, cfg.REALTOR_PASSWORD )
        # time.sleep( 3 ) #secs

        wait.until( apartments_table.visible() )
        wait.until( apartments_table.expected_to_contain_row_with_text( 'apartment A' ) )

        apartments_table.show_add_form()
        wait.until( apartments_form.visible() )

        apartments_form.fill_form( 'new name', 'description', 100, 100, 2, 'address', 50, 50, None, 0 )
        # time.sleep( 10 ) #secs
        apartments_form.do_add()

        wait.until( apartments_table.visible() )
        wait.until( apartments_table.expected_to_contain_row_with_text( 'new name' ) )

        app_helper.logout()


    def test_update(self):
        driver = self.driver
        driver.implicitly_wait( 5 ) # seconds
        wait = WebDriverWait( driver, 5 ) # seconds
        DBResetApartments()

        app_helper = AppHelper( driver )
        apartments_form = ApartmentsForm( driver )
        apartments_table = ApartmentsTable( driver )

        app_helper.login( cfg.REALTOR_IDENTITY, cfg.REALTOR_PASSWORD )
        # time.sleep( 3 ) #secs

        wait.until( apartments_table.visible() )
        wait.until( apartments_table.expected_to_contain_row_with_text( 'apartment A' ) )

        elem = apartments_table.find_row_with_text( 'apartment A' )
        btn = apartments_table.find_row_detail_button( elem )
        btn.click()

        apartments_form = ApartmentsForm( driver )
        wait.until( apartments_form.visible() )

        apartments_form.fill_form( 'apartment updated', None, None, None, None, None, None, None, None, None )
        # time.sleep( 5 ) #secs
        apartments_form.do_update()

        wait.until( apartments_table.expected_to_contain_row_with_text( 'apartment updated' ) )
        # time.sleep( 5 ) #secs
        self.assertTrue( apartments_table.doesnt_contain_row_with_text( 'apartment A' ) )

        app_helper.logout()


    def test_delete(self):
        driver = self.driver
        driver.implicitly_wait( 5 ) # seconds
        wait = WebDriverWait( driver, 5 ) # seconds
        DBResetApartments()

        app_helper = AppHelper( driver )
        apartments_table = ApartmentsTable( driver )

        app_helper.login( cfg.REALTOR_IDENTITY, cfg.REALTOR_PASSWORD )
        # time.sleep( 3 ) #secs

        wait.until( apartments_table.visible() )
        wait.until( apartments_table.expected_to_contain_row_with_text( 'apartment A' ) )

        elem = apartments_table.find_row_with_text( 'apartment A' )
        elem = apartments_table.find_row_delete_button( elem )
        elem.click()

        driver.switch_to_alert()
        Alert(driver).accept()

        wait.until( apartments_table.expected_to_not_contain_row_with_text( 'apartment A' ) )
        # time.sleep( 5 ) #secs

        app_helper.logout()


    def tearDown(self):
        self.driver.close()

