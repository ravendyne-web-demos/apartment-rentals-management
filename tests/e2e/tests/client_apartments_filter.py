## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later

import e2e_config as cfg

import unittest

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

import time

from lib.app_helpers import DBResetData, DBInsertApartment, ISODateString
from lib.app_helpers import AppHelper

from lib.apartments import ApartmentsFilterForm, ApartmentsTable
from lib.locators import AppFormLocators


class ClientApartmentsFilter(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox()
        DBResetData()


    def test_filter_reset(self):
        driver = self.driver
        driver.implicitly_wait( 5 ) # seconds
        wait = WebDriverWait( driver, 5 ) # seconds

        DBInsertApartment( 'my apartment A', 'apartment A description', 300, 30, 3, 49.0, 50.0, ISODateString( 2019, 6, 14 ), cfg.REALTOR_DB_ID, 0, 'address A' )

        app_helper = AppHelper( driver )
        app_helper.login( cfg.CLIENT_IDENTITY, cfg.CLIENT_PASSWORD )
        # time.sleep( 3 ) #secs

        apartments_filter_form = ApartmentsFilterForm( driver )
        apartments_filter_form.toggle_filter_form()
        wait.until( apartments_filter_form.visible() )

        apartments_filter_form.fill_form( 100, 400, 50, 100, 1, 3 )
        # time.sleep( 3 ) #secs

        form_values = apartments_filter_form.get_field_values()
        self.assertTrue( form_values[0] )
        self.assertTrue( form_values[1] )
        self.assertTrue( form_values[2] )
        self.assertTrue( form_values[3] )
        self.assertTrue( form_values[4] )
        self.assertTrue( form_values[5] )

        apartments_filter_form.do_reset()
        # time.sleep( 3 ) #secs

        form_values = apartments_filter_form.get_field_values()
        self.assertFalse( form_values[0] )
        self.assertFalse( form_values[1] )
        self.assertFalse( form_values[2] )
        self.assertFalse( form_values[3] )
        self.assertFalse( form_values[4] )
        self.assertFalse( form_values[5] )

        app_helper.logout()


    def test_display_all(self):
        driver = self.driver
        driver.implicitly_wait( 5 ) # seconds
        wait = WebDriverWait( driver, 5 ) # seconds

        # DBInsertApartment( name, description, floor_area_size, price_per_month, number_of_rooms, geolocation_lat, geolocation_long, date_added, associated_realtor, is_rented, address ):
        DBInsertApartment( 'my apartment A', 'apartment A description', 300, 30, 3, 49.0, 50.0, ISODateString( 2019, 6, 14 ), cfg.REALTOR_DB_ID, 0, 'address A' )
        DBInsertApartment( 'my apartment B', 'apartment B description', 400, 40, 4, 49.0, 50.0, ISODateString( 2019, 6, 14 ), cfg.REALTOR_DB_ID, 0, 'address B' )
        DBInsertApartment( 'my apartment C', 'apartment C description', 500, 50, 5, 49.0, 50.0, ISODateString( 2019, 6, 14 ), cfg.REALTOR_DB_ID, 0, 'address C' )
        DBInsertApartment( 'my apartment D', 'apartment D description', 600, 60, 6, 49.0, 50.0, ISODateString( 2019, 6, 14 ), cfg.REALTOR_DB_ID, 0, 'address D' )

        app_helper = AppHelper( driver )
        apartments_filter_form = ApartmentsFilterForm( driver )
        apartments_table = ApartmentsTable( driver )

        app_helper.login( cfg.CLIENT_IDENTITY, cfg.CLIENT_PASSWORD )
        # time.sleep( 3 ) #secs

        apartments_filter_form.toggle_filter_form()
        wait.until( apartments_filter_form.visible() )

        apartments_filter_form.do_reset()
        apartments_filter_form.do_filter()
        # time.sleep( 3 ) #secs

        wait.until( apartments_table.expected_to_contain_row_with_text( 'my apartment A' ) )

        self.assertTrue( apartments_table.contains_row_with_text( 'my apartment B' ) )
        self.assertTrue( apartments_table.contains_row_with_text( 'my apartment C' ) )
        self.assertTrue( apartments_table.contains_row_with_text( 'my apartment D' ) )

        app_helper.logout()


    def test_filter_size_from(self):
        driver = self.driver
        driver.implicitly_wait( 5 ) # seconds
        wait = WebDriverWait( driver, 5 ) # seconds

        # DBInsertApartment( name, description, floor_area_size, price_per_month, number_of_rooms, geolocation_lat, geolocation_long, date_added, associated_realtor, is_rented, address ):
        DBInsertApartment( 'my apartment A', 'apartment A description', 300, 30, 3, 49.0, 50.0, ISODateString( 2019, 6, 14 ), cfg.REALTOR_DB_ID, 0, 'address A' )
        DBInsertApartment( 'my apartment B', 'apartment B description', 400, 40, 4, 49.0, 50.0, ISODateString( 2019, 6, 14 ), cfg.REALTOR_DB_ID, 0, 'address B' )
        DBInsertApartment( 'my apartment C', 'apartment C description', 500, 50, 5, 49.0, 50.0, ISODateString( 2019, 6, 14 ), cfg.REALTOR_DB_ID, 0, 'address C' )
        DBInsertApartment( 'my apartment D', 'apartment D description', 600, 60, 6, 49.0, 50.0, ISODateString( 2019, 6, 14 ), cfg.REALTOR_DB_ID, 0, 'address D' )

        app_helper = AppHelper( driver )
        apartments_filter_form = ApartmentsFilterForm( driver )
        apartments_table = ApartmentsTable( driver )

        app_helper.login( cfg.CLIENT_IDENTITY, cfg.CLIENT_PASSWORD )
        # time.sleep( 3 ) #secs

        apartments_filter_form.toggle_filter_form()
        wait.until( apartments_filter_form.visible() )

        apartments_filter_form.do_reset()
        apartments_filter_form.fill_form( 350, None, None, None, None, None )
        apartments_filter_form.do_filter()
        # time.sleep( 3 ) #secs

        wait.until( apartments_table.expected_to_contain_row_with_text( 'my apartment B' ) )

        self.assertTrue( apartments_table.contains_row_with_text( 'my apartment C' ) )
        self.assertTrue( apartments_table.contains_row_with_text( 'my apartment D' ) )

        self.assertFalse( apartments_table.contains_row_with_text( 'my apartment A' ) )

        app_helper.logout()


    def test_filter_size_to(self):
        driver = self.driver
        driver.implicitly_wait( 5 ) # seconds
        wait = WebDriverWait( driver, 5 ) # seconds

        # DBInsertApartment( name, description, floor_area_size, price_per_month, number_of_rooms, geolocation_lat, geolocation_long, date_added, associated_realtor, is_rented, address ):
        DBInsertApartment( 'my apartment A', 'apartment A description', 300, 30, 3, 49.0, 50.0, ISODateString( 2019, 6, 14 ), cfg.REALTOR_DB_ID, 0, 'address A' )
        DBInsertApartment( 'my apartment B', 'apartment B description', 400, 40, 4, 49.0, 50.0, ISODateString( 2019, 6, 14 ), cfg.REALTOR_DB_ID, 0, 'address B' )
        DBInsertApartment( 'my apartment C', 'apartment C description', 500, 50, 5, 49.0, 50.0, ISODateString( 2019, 6, 14 ), cfg.REALTOR_DB_ID, 0, 'address C' )
        DBInsertApartment( 'my apartment D', 'apartment D description', 600, 60, 6, 49.0, 50.0, ISODateString( 2019, 6, 14 ), cfg.REALTOR_DB_ID, 0, 'address D' )

        app_helper = AppHelper( driver )
        apartments_filter_form = ApartmentsFilterForm( driver )
        apartments_table = ApartmentsTable( driver )

        app_helper.login( cfg.CLIENT_IDENTITY, cfg.CLIENT_PASSWORD )
        # time.sleep( 3 ) #secs

        apartments_filter_form.toggle_filter_form()
        wait.until( apartments_filter_form.visible() )

        apartments_filter_form.do_reset()
        apartments_filter_form.fill_form( None, 450, None, None, None, None )
        apartments_filter_form.do_filter()
        # time.sleep( 3 ) #secs

        wait.until( apartments_table.expected_to_not_contain_row_with_text( 'my apartment C' ) )

        self.assertTrue( apartments_table.contains_row_with_text( 'my apartment B' ) )
        self.assertTrue( apartments_table.contains_row_with_text( 'my apartment A' ) )

        self.assertFalse( apartments_table.contains_row_with_text( 'my apartment C' ) )
        self.assertFalse( apartments_table.contains_row_with_text( 'my apartment D' ) )

        app_helper.logout()


    def test_filter_price_from(self):
        driver = self.driver
        driver.implicitly_wait( 5 ) # seconds
        wait = WebDriverWait( driver, 5 ) # seconds

        # DBInsertApartment( name, description, floor_area_size, price_per_month, number_of_rooms, geolocation_lat, geolocation_long, date_added, associated_realtor, is_rented, address ):
        DBInsertApartment( 'my apartment A', 'apartment A description', 300, 30, 3, 49.0, 50.0, ISODateString( 2019, 6, 14 ), cfg.REALTOR_DB_ID, 0, 'address A' )
        DBInsertApartment( 'my apartment B', 'apartment B description', 400, 40, 4, 49.0, 50.0, ISODateString( 2019, 6, 14 ), cfg.REALTOR_DB_ID, 0, 'address B' )
        DBInsertApartment( 'my apartment C', 'apartment C description', 500, 50, 5, 49.0, 50.0, ISODateString( 2019, 6, 14 ), cfg.REALTOR_DB_ID, 0, 'address C' )
        DBInsertApartment( 'my apartment D', 'apartment D description', 600, 60, 6, 49.0, 50.0, ISODateString( 2019, 6, 14 ), cfg.REALTOR_DB_ID, 0, 'address D' )

        app_helper = AppHelper( driver )
        apartments_filter_form = ApartmentsFilterForm( driver )
        apartments_table = ApartmentsTable( driver )

        app_helper.login( cfg.CLIENT_IDENTITY, cfg.CLIENT_PASSWORD )
        # time.sleep( 3 ) #secs

        apartments_filter_form.toggle_filter_form()
        wait.until( apartments_filter_form.visible() )

        apartments_filter_form.do_reset()
        apartments_filter_form.fill_form( None, None, 35, None, None, None )
        apartments_filter_form.do_filter()
        # time.sleep( 3 ) #secs

        wait.until( apartments_table.expected_to_contain_row_with_text( 'my apartment B' ) )

        self.assertTrue( apartments_table.contains_row_with_text( 'my apartment C' ) )
        self.assertTrue( apartments_table.contains_row_with_text( 'my apartment D' ) )

        self.assertFalse( apartments_table.contains_row_with_text( 'my apartment A' ) )

        app_helper.logout()


    def test_filter_price_to(self):
        driver = self.driver
        driver.implicitly_wait( 5 ) # seconds
        wait = WebDriverWait( driver, 5 ) # seconds

        # DBInsertApartment( name, description, floor_area_size, price_per_month, number_of_rooms, geolocation_lat, geolocation_long, date_added, associated_realtor, is_rented, address ):
        DBInsertApartment( 'my apartment A', 'apartment A description', 300, 30, 3, 49.0, 50.0, ISODateString( 2019, 6, 14 ), cfg.REALTOR_DB_ID, 0, 'address A' )
        DBInsertApartment( 'my apartment B', 'apartment B description', 400, 40, 4, 49.0, 50.0, ISODateString( 2019, 6, 14 ), cfg.REALTOR_DB_ID, 0, 'address B' )
        DBInsertApartment( 'my apartment C', 'apartment C description', 500, 50, 5, 49.0, 50.0, ISODateString( 2019, 6, 14 ), cfg.REALTOR_DB_ID, 0, 'address C' )
        DBInsertApartment( 'my apartment D', 'apartment D description', 600, 60, 6, 49.0, 50.0, ISODateString( 2019, 6, 14 ), cfg.REALTOR_DB_ID, 0, 'address D' )

        app_helper = AppHelper( driver )
        apartments_filter_form = ApartmentsFilterForm( driver )
        apartments_table = ApartmentsTable( driver )

        app_helper.login( cfg.CLIENT_IDENTITY, cfg.CLIENT_PASSWORD )
        # time.sleep( 3 ) #secs

        apartments_filter_form.toggle_filter_form()
        wait.until( apartments_filter_form.visible() )

        apartments_filter_form.do_reset()
        apartments_filter_form.fill_form( None, None, None, 35, None, None )
        apartments_filter_form.do_filter()
        # time.sleep( 3 ) #secs

        wait.until( apartments_table.expected_to_contain_row_with_text( 'my apartment A' ) )

        self.assertFalse( apartments_table.contains_row_with_text( 'my apartment B' ) )
        self.assertFalse( apartments_table.contains_row_with_text( 'my apartment C' ) )
        self.assertFalse( apartments_table.contains_row_with_text( 'my apartment D' ) )

        app_helper.logout()


    def test_filter_rooms_from(self):
        driver = self.driver
        driver.implicitly_wait( 5 ) # seconds
        wait = WebDriverWait( driver, 5 ) # seconds

        # DBInsertApartment( name, description, floor_area_size, price_per_month, number_of_rooms, geolocation_lat, geolocation_long, date_added, associated_realtor, is_rented, address ):
        DBInsertApartment( 'my apartment A', 'apartment A description', 300, 30, 3, 49.0, 50.0, ISODateString( 2019, 6, 14 ), cfg.REALTOR_DB_ID, 0, 'address A' )
        DBInsertApartment( 'my apartment B', 'apartment B description', 400, 40, 4, 49.0, 50.0, ISODateString( 2019, 6, 14 ), cfg.REALTOR_DB_ID, 0, 'address B' )
        DBInsertApartment( 'my apartment C', 'apartment C description', 500, 50, 5, 49.0, 50.0, ISODateString( 2019, 6, 14 ), cfg.REALTOR_DB_ID, 0, 'address C' )
        DBInsertApartment( 'my apartment D', 'apartment D description', 600, 60, 6, 49.0, 50.0, ISODateString( 2019, 6, 14 ), cfg.REALTOR_DB_ID, 0, 'address D' )

        app_helper = AppHelper( driver )
        apartments_filter_form = ApartmentsFilterForm( driver )
        apartments_table = ApartmentsTable( driver )

        app_helper.login( cfg.CLIENT_IDENTITY, cfg.CLIENT_PASSWORD )
        # time.sleep( 3 ) #secs

        apartments_filter_form.toggle_filter_form()
        wait.until( apartments_filter_form.visible() )

        apartments_filter_form.do_reset()
        apartments_filter_form.fill_form( None, None, None, None, 4, None )
        apartments_filter_form.do_filter()
        # time.sleep( 3 ) #secs

        wait.until( apartments_table.expected_to_contain_row_with_text( 'my apartment B' ) )

        self.assertTrue( apartments_table.contains_row_with_text( 'my apartment C' ) )
        self.assertTrue( apartments_table.contains_row_with_text( 'my apartment D' ) )

        self.assertFalse( apartments_table.contains_row_with_text( 'my apartment A' ) )

        app_helper.logout()


    def test_filter_rooms_to(self):
        driver = self.driver
        driver.implicitly_wait( 5 ) # seconds
        wait = WebDriverWait( driver, 5 ) # seconds

        # DBInsertApartment( name, description, floor_area_size, price_per_month, number_of_rooms, geolocation_lat, geolocation_long, date_added, associated_realtor, is_rented, address ):
        DBInsertApartment( 'my apartment A', 'apartment A description', 300, 30, 3, 49.0, 50.0, ISODateString( 2019, 6, 14 ), cfg.REALTOR_DB_ID, 0, 'address A' )
        DBInsertApartment( 'my apartment B', 'apartment B description', 400, 40, 4, 49.0, 50.0, ISODateString( 2019, 6, 14 ), cfg.REALTOR_DB_ID, 0, 'address B' )
        DBInsertApartment( 'my apartment C', 'apartment C description', 500, 50, 5, 49.0, 50.0, ISODateString( 2019, 6, 14 ), cfg.REALTOR_DB_ID, 0, 'address C' )
        DBInsertApartment( 'my apartment D', 'apartment D description', 600, 60, 6, 49.0, 50.0, ISODateString( 2019, 6, 14 ), cfg.REALTOR_DB_ID, 0, 'address D' )

        app_helper = AppHelper( driver )
        apartments_filter_form = ApartmentsFilterForm( driver )
        apartments_table = ApartmentsTable( driver )

        app_helper.login( cfg.CLIENT_IDENTITY, cfg.CLIENT_PASSWORD )
        # time.sleep( 3 ) #secs

        apartments_filter_form.toggle_filter_form()
        wait.until( apartments_filter_form.visible() )

        apartments_filter_form.do_reset()
        apartments_filter_form.fill_form( None, None, None, None, None, 3 )
        apartments_filter_form.do_filter()
        # time.sleep( 3 ) #secs

        wait.until( apartments_table.expected_to_contain_row_with_text( 'my apartment A' ) )

        self.assertFalse( apartments_table.contains_row_with_text( 'my apartment B' ) )
        self.assertFalse( apartments_table.contains_row_with_text( 'my apartment C' ) )
        self.assertFalse( apartments_table.contains_row_with_text( 'my apartment D' ) )

        app_helper.logout()


    def test_filter_size_price_rooms(self):
        driver = self.driver
        driver.implicitly_wait( 5 ) # seconds
        wait = WebDriverWait( driver, 5 ) # seconds

        # DBInsertApartment( name, description, floor_area_size, price_per_month, number_of_rooms, geolocation_lat, geolocation_long, date_added, associated_realtor, is_rented, address ):
        DBInsertApartment( 'my apartment A', 'apartment A description', 300, 30, 3, 49.0, 50.0, ISODateString( 2019, 6, 14 ), cfg.REALTOR_DB_ID, 0, 'address A' )
        DBInsertApartment( 'my apartment B', 'apartment B description', 400, 40, 4, 49.0, 50.0, ISODateString( 2019, 6, 14 ), cfg.REALTOR_DB_ID, 0, 'address B' )
        DBInsertApartment( 'my apartment C', 'apartment C description', 500, 50, 5, 49.0, 50.0, ISODateString( 2019, 6, 14 ), cfg.REALTOR_DB_ID, 0, 'address C' )
        DBInsertApartment( 'my apartment D', 'apartment D description', 600, 60, 6, 49.0, 50.0, ISODateString( 2019, 6, 14 ), cfg.REALTOR_DB_ID, 0, 'address D' )
        DBInsertApartment( 'my apartment E', 'apartment E description', 700, 70, 7, 49.0, 50.0, ISODateString( 2019, 6, 14 ), cfg.REALTOR_DB_ID, 0, 'address E' )
        DBInsertApartment( 'my apartment F', 'apartment F description', 800, 80, 8, 49.0, 50.0, ISODateString( 2019, 6, 14 ), cfg.REALTOR_DB_ID, 0, 'address F' )

        app_helper = AppHelper( driver )
        apartments_filter_form = ApartmentsFilterForm( driver )
        apartments_table = ApartmentsTable( driver )

        app_helper.login( cfg.CLIENT_IDENTITY, cfg.CLIENT_PASSWORD )
        # time.sleep( 3 ) #secs

        apartments_filter_form.toggle_filter_form()
        wait.until( apartments_filter_form.visible() )

        apartments_filter_form.do_reset()
        apartments_filter_form.fill_form(
            300, 700,
            40, 70,
            4, 6
        )
        apartments_filter_form.do_filter()
        # time.sleep( 3 ) #secs

        wait.until( apartments_table.expected_to_contain_row_with_text( 'my apartment B' ) )

        self.assertTrue( apartments_table.contains_row_with_text( 'my apartment C' ) )
        self.assertTrue( apartments_table.contains_row_with_text( 'my apartment D' ) )

        self.assertFalse( apartments_table.contains_row_with_text( 'my apartment A' ) )
        self.assertFalse( apartments_table.contains_row_with_text( 'my apartment E' ) )
        self.assertFalse( apartments_table.contains_row_with_text( 'my apartment F' ) )

        app_helper.logout()


    def tearDown(self):
        self.driver.close()

