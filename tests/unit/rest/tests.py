## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later

import unittest

# import all test modules
import tests.api_login
import tests.api_apartments
import tests.api_users


# initialize the test suite
loader = unittest.TestLoader()
suite  = unittest.TestSuite()

# add tests from modules to the test suite
suite.addTests( loader.loadTestsFromModule( tests.api_login ) )
suite.addTests( loader.loadTestsFromModule( tests.api_apartments ) )
suite.addTests( loader.loadTestsFromModule( tests.api_users ) )

# initialize a runner and run our test suite with it
runner = unittest.TextTestRunner( verbosity = 3 )
result = runner.run( suite )
