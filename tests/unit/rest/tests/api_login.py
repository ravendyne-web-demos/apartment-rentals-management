## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later

import rest_config as cfg

import unittest
import requests
import json
from api_test_base import ApiTestBase


class ApiAuthWhoami(ApiTestBase):

    def test_whoami_before_login(self):
        url = cfg.API_BASE_URL + cfg.API_AUTH_WHOAMI
        headers = { 'Content-Type': 'application/json' }

        r = requests.get( url, headers=headers )

        self.assertEqual( r.status_code, 401 )
        self.assertEqual( r.headers['Content-Type'], 'application/json' )

        data = r.json()
        self.assertEqual( data['success'], False )
        self.assertEqual( data['needs_login'], True )


    def test_whoami_after_login(self):
        url = cfg.API_BASE_URL + cfg.API_AUTH_WHOAMI
        headers = { 'Content-Type': 'application/json' }

        #
        # login
        #
        payload = {
            'identity' : 'client@arm.com',
            'password' : 'password'
        }
        r = requests.post( cfg.API_BASE_URL + cfg.API_AUTH_LOGIN, headers=headers, data=json.dumps( payload ) )
        self.assertEqual( r.status_code, 200 )
        cookies = r.cookies

        #
        # test
        #
        r = requests.get( url, headers=headers, cookies=cookies )

        self.assertEqual( r.status_code, 200 )
        self.assertEqual( r.headers['Content-Type'], 'application/json' )

        data = r.json()
        self.assertEqual( data['success'], True )
        self.assertTrue( 'user' in data )

        #
        # logout
        #
        r = requests.post( cfg.API_BASE_URL + cfg.API_AUTH_LOGOUT, headers=headers, data='{}', cookies=cookies )
        self.assertEqual( r.status_code, 200 )


class ApiAuthLogin(ApiTestBase):

    def test_login_without_credentials(self):
        url = cfg.API_BASE_URL + cfg.API_AUTH_LOGIN
        headers = { 'Content-Type': 'application/json' }

        r = requests.post( url, headers=headers )

        self.assertEqual( r.status_code, 400 )
        self.assertEqual( r.headers['Content-Type'], 'application/json' )

        data = r.json()
        self.assertEqual( data['success'], False )
        self.assertTrue( 'message' in data )


    def test_login_with_credentials(self):
        url = cfg.API_BASE_URL + cfg.API_AUTH_LOGIN
        headers = { 'Content-Type': 'application/json' }
        payload = {
            'identity' : 'client@arm.com',
            'password' : 'password'
        }

        r = requests.post( url, headers=headers, data=json.dumps( payload ) )

        self.assertEqual( r.status_code, 200 )
        self.assertEqual( r.headers['Content-Type'], 'application/json' )

        data = r.json()
        self.assertEqual( data['success'], True )
        self.assertTrue( 'user' in data )

        user_data = data['user']
        self.assertTrue( 'id' in user_data )
        self.assertTrue( 'username' in user_data )
        self.assertTrue( 'email' in user_data )
        self.assertTrue( 'groups' in user_data )
        self.assertTrue( 'is_admin' in user_data )

        cookies = r.cookies
        r = requests.post( cfg.API_BASE_URL + cfg.API_AUTH_LOGOUT, headers=headers, data='{}', cookies=cookies )
        self.assertEqual( r.status_code, 200 )


    def test_login_as_client(self):
        url = cfg.API_BASE_URL + cfg.API_AUTH_LOGIN
        headers = { 'Content-Type': 'application/json' }
        payload = {
            'identity' : 'client@arm.com',
            'password' : 'password'
        }

        r = requests.post( url, headers=headers, data=json.dumps( payload ) )

        self.assertEqual( r.status_code, 200 )

        data = r.json()
        user_data = data['user']
        self.assertTrue( 'client' in user_data['groups'] )
        self.assertEqual( user_data['is_admin'], False )

        cookies = r.cookies
        r = requests.post( cfg.API_BASE_URL + cfg.API_AUTH_LOGOUT, headers=headers, data='{}', cookies=cookies )
        self.assertEqual( r.status_code, 200 )


    def test_login_as_realtor(self):
        url = cfg.API_BASE_URL + cfg.API_AUTH_LOGIN
        headers = { 'Content-Type': 'application/json' }
        payload = {
            'identity' : 'realtor@arm.com',
            'password' : 'password'
        }

        r = requests.post( url, headers=headers, data=json.dumps( payload ) )

        self.assertEqual( r.status_code, 200 )

        data = r.json()
        user_data = data['user']
        self.assertTrue( 'realtor' in user_data['groups'] )
        self.assertEqual( user_data['is_admin'], False )

        cookies = r.cookies
        r = requests.post( cfg.API_BASE_URL + cfg.API_AUTH_LOGOUT, headers=headers, data='{}', cookies=cookies )
        self.assertEqual( r.status_code, 200 )


    def test_login_as_admin(self):
        url = cfg.API_BASE_URL + cfg.API_AUTH_LOGIN
        headers = { 'Content-Type': 'application/json' }
        payload = {
            'identity' : 'admin@arm.com',
            'password' : 'password'
        }

        r = requests.post( url, headers=headers, data=json.dumps( payload ) )

        self.assertEqual( r.status_code, 200 )

        data = r.json()
        user_data = data['user']
        self.assertTrue( 'admin' in user_data['groups'] )
        self.assertEqual( user_data['is_admin'], True )

        cookies = r.cookies
        r = requests.post( cfg.API_BASE_URL + cfg.API_AUTH_LOGOUT, headers=headers, data='{}', cookies=cookies )
        self.assertEqual( r.status_code, 200 )

