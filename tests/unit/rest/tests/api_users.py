## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later

import rest_config as cfg

import unittest
import requests
import json
from api_test_base import ApiTestBase


class ApiUsersRetrieve(ApiTestBase):

    def setUp(self):
        ApiTestBase.setUp(self)


    def test_retrieve_before_login(self):
        url = cfg.API_BASE_URL + cfg.API_USERS
        user_id = '2'
        r = requests.get( url + '/' + user_id, headers=self.headers, cookies=self.cookies )

        self.assertEqual( r.status_code, 401 )
        self.assertEqual( r.headers['Content-Type'], 'application/json' )

        data = r.json()
        self.assertEqual( data['success'], False )
        self.assertEqual( data['needs_login'], True )


    def test_retrieve_as_client(self):

        self.do_login_client()

        url = cfg.API_BASE_URL + cfg.API_USERS
        user_id = '2'
        r = requests.get( url + '/' + user_id, headers=self.headers, cookies=self.cookies )

        self.assertEqual( r.status_code, 401 )
        self.assertEqual( r.headers['Content-Type'], 'application/json' )

        data = r.json()
        self.assertEqual( data['success'], False )

        self.do_logout()


    def test_retrieve_as_realtor(self):

        self.do_login_client()

        url = cfg.API_BASE_URL + cfg.API_USERS
        user_id = '2'
        r = requests.get( url + '/' + user_id, headers=self.headers, cookies=self.cookies )

        self.assertEqual( r.status_code, 401 )
        self.assertEqual( r.headers['Content-Type'], 'application/json' )

        data = r.json()
        self.assertEqual( data['success'], False )

        self.do_logout()


    def test_retrieve_as_admin(self):

        self.do_login_admin()

        url = cfg.API_BASE_URL + cfg.API_USERS
        user_id = '2'
        r = requests.get( url + '/' + user_id, headers=self.headers, cookies=self.cookies )

        self.assertEqual( r.status_code, 200 )
        self.assertEqual( r.headers['Content-Type'], 'application/json' )

        data = r.json()
        self.assertEqual( data['success'], True )
        self.assertTrue( 'user' in data )

        self.do_logout()


    def test_retrieve_all(self):

        self.do_login_admin()

        url = cfg.API_BASE_URL + cfg.API_USERS
        r = requests.get( url, headers=self.headers, cookies=self.cookies )

        self.assertEqual( r.status_code, 200 )
        self.assertEqual( r.headers['Content-Type'], 'application/json' )

        data = r.json()
        self.assertEqual( data['success'], True )
        self.assertTrue( 'users' in data )

        data_users = data['users']
        self.assertGreater( len( data_users ), 0 )

        self.do_logout()


    def test_retrieve_realtors(self):

        self.do_login_admin()

        url = cfg.API_BASE_URL + cfg.API_USERS + '/realtors'
        r = requests.get( url, headers=self.headers, cookies=self.cookies )

        self.assertEqual( r.status_code, 200 )
        self.assertEqual( r.headers['Content-Type'], 'application/json' )

        data = r.json()
        self.assertEqual( data['success'], True )
        self.assertTrue( 'users' in data )

        data_users = data['users']
        self.assertGreater( len( data_users ), 0 )
        for user in data_users:
            self.assertTrue( user['realtor'] )

        self.do_logout()


class ApiUsersCreate(ApiTestBase):

    def setUp(self):
        ApiTestBase.setUp(self)


    def test_create_before_login(self):
        url = cfg.API_BASE_URL + cfg.API_USERS
        payload = {
            "never_mind": "don't matter"
        }
        r = requests.post( url, data=payload, headers=self.headers, cookies=self.cookies )

        self.assertEqual( r.status_code, 401 )
        self.assertEqual( r.headers['Content-Type'], 'application/json' )

        data = r.json()
        self.assertEqual( data['success'], False )
        self.assertEqual( data['needs_login'], True )


    def test_create_as_client(self):

        self.do_login_client()

        url = cfg.API_BASE_URL + cfg.API_USERS
        payload = {
            "never_mind": "don't matter"
        }
        r = requests.post( url, data=json.dumps( payload ), headers=self.headers, cookies=self.cookies )

        self.assertEqual( r.status_code, 401 )
        self.assertEqual( r.headers['Content-Type'], 'application/json' )

        data = r.json()
        self.assertEqual( data['success'], False )

        self.do_logout()


    def test_create_as_realtor(self):

        self.do_login_realtor()

        url = cfg.API_BASE_URL + cfg.API_USERS
        payload = {
            "never_mind": "don't matter"
        }
        r = requests.post( url, data=json.dumps( payload ), headers=self.headers, cookies=self.cookies )

        self.assertEqual( r.status_code, 401 )
        self.assertEqual( r.headers['Content-Type'], 'application/json' )

        data = r.json()
        self.assertEqual( data['success'], False )

        self.do_logout()


    def test_create_as_admin(self):

        self.do_login_admin()

        url = cfg.API_BASE_URL + cfg.API_USERS
        payload = {
            "username": "client_user_next",
            "password": "password",
            "email": "client_next@arm.com",
            "realtor": False
        }
        r = requests.post( url, data=json.dumps( payload ), headers=self.headers, cookies=self.cookies )

        self.assertEqual( r.status_code, 201 )
        self.assertEqual( r.headers['Content-Type'], 'application/json' )

        data = r.json()
        self.assertEqual( data['success'], True )
        self.assertTrue( 'location' in data )
        self.assertTrue( 'Location' in r.headers )

        self.do_logout()


class ApiUsersUpdate(ApiTestBase):

    def setUp(self):
        ApiTestBase.setUp(self)


    def test_update_before_login(self):
        url = cfg.API_BASE_URL + cfg.API_USERS
        payload = {
            "never_mind": "don't matter"
        }
        r = requests.put( url, data=payload, headers=self.headers, cookies=self.cookies )

        self.assertEqual( r.status_code, 401 )
        self.assertEqual( r.headers['Content-Type'], 'application/json' )

        data = r.json()
        self.assertEqual( data['success'], False )
        self.assertEqual( data['needs_login'], True )


    def test_update_as_client(self):

        self.do_login_client()

        url = cfg.API_BASE_URL + cfg.API_USERS
        payload = {
            "never_mind": "don't matter"
        }
        r = requests.put( url, data=json.dumps( payload ), headers=self.headers, cookies=self.cookies )

        self.assertEqual( r.status_code, 401 )
        self.assertEqual( r.headers['Content-Type'], 'application/json' )

        data = r.json()
        self.assertEqual( data['success'], False )

        self.do_logout()


    def test_update_as_realtor(self):

        self.do_login_realtor()

        url = cfg.API_BASE_URL + cfg.API_USERS
        payload = {
            "never_mind": "don't matter"
        }
        r = requests.put( url, data=json.dumps( payload ), headers=self.headers, cookies=self.cookies )

        self.assertEqual( r.status_code, 401 )
        self.assertEqual( r.headers['Content-Type'], 'application/json' )

        data = r.json()
        self.assertEqual( data['success'], False )

        self.do_logout()


    def test_update_as_admin(self):

        self.do_login_admin()

        url = cfg.API_BASE_URL + cfg.API_USERS
        user_id = '2'
        payload = {
            "password": "password123",
            "realtor": True
        }
        r = requests.put( url + '/' + user_id, data=json.dumps( payload ), headers=self.headers, cookies=self.cookies )

        self.assertEqual( r.status_code, 200 )
        self.assertEqual( r.headers['Content-Type'], 'application/json' )

        data = r.json()
        self.assertEqual( data['success'], True )

        self.do_logout()


class ApiUsersDelete(ApiTestBase):

    def setUp(self):
        ApiTestBase.setUp(self)


    def test_delete_before_login(self):
        url = cfg.API_BASE_URL + cfg.API_USERS
        r = requests.delete( url, headers=self.headers, cookies=self.cookies )

        self.assertEqual( r.status_code, 401 )
        self.assertEqual( r.headers['Content-Type'], 'application/json' )

        data = r.json()
        self.assertEqual( data['success'], False )
        self.assertEqual( data['needs_login'], True )


    def test_delete_as_client(self):

        self.do_login_client()

        url = cfg.API_BASE_URL + cfg.API_USERS
        user_id = '4'
        r = requests.delete( url + '/' + user_id, headers=self.headers, cookies=self.cookies )

        self.assertEqual( r.status_code, 401 )
        self.assertEqual( r.headers['Content-Type'], 'application/json' )

        data = r.json()
        self.assertEqual( data['success'], False )

        self.do_logout()


    def test_delete_as_realtor(self):

        self.do_login_realtor()

        url = cfg.API_BASE_URL + cfg.API_USERS
        user_id = '4'
        r = requests.delete( url + '/' + user_id, headers=self.headers, cookies=self.cookies )

        self.assertEqual( r.status_code, 401 )
        self.assertEqual( r.headers['Content-Type'], 'application/json' )

        data = r.json()
        self.assertEqual( data['success'], False )

        self.do_logout()


    def test_delete_as_admin(self):

        self.do_login_admin()

        url = cfg.API_BASE_URL + cfg.API_USERS
        user_id = '4'
        r = requests.delete( url + '/' + user_id, headers=self.headers, cookies=self.cookies )

        self.assertEqual( r.status_code, 200 )
        self.assertEqual( r.headers['Content-Type'], 'application/json' )

        data = r.json()
        self.assertEqual( data['success'], True )

        self.do_logout()
