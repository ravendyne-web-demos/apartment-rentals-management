## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later

import rest_config as cfg

import unittest
import requests
import json
from api_test_base import ApiTestBase


class ApiApartmentsRetrieve(ApiTestBase):

    def setUp(self):
        ApiTestBase.setUp(self)
        self.db_load_apartments()


    def test_retrieve_before_login(self):
        url = cfg.API_BASE_URL + cfg.API_APARTMENTS
        apt_id = '101'
        r = requests.get( url + '/' + apt_id, headers=self.headers, cookies=self.cookies )

        self.assertEqual( r.status_code, 401 )
        self.assertEqual( r.headers['Content-Type'], 'application/json' )

        data = r.json()
        self.assertEqual( data['success'], False )
        self.assertEqual( data['needs_login'], True )


    def test_retrieve(self):

        self.do_login_client()

        url = cfg.API_BASE_URL + cfg.API_APARTMENTS
        apt_id = '100'
        r = requests.get( url + '/' + apt_id, headers=self.headers, cookies=self.cookies )

        self.assertEqual( r.status_code, 200 )
        self.assertEqual( r.headers['Content-Type'], 'application/json' )

        data = r.json()
        self.assertEqual( data['success'], True )
        self.assertTrue( 'apartments' in data )
        self.assertTrue( 'filter' in data )

        self.do_logout()


    def test_retrieve_with_filter(self):

        self.do_login_client()

        url = cfg.API_BASE_URL + cfg.API_APARTMENTS
        payload = {
            "filter": {
                "size_from": 100,
                "size_to": 500,
                "price_from": 40,
                "price_to": 120,
                "rooms_from": 1,
                "rooms_to": 2
            }
        }
        r = requests.get( url, data=payload, headers=self.headers, cookies=self.cookies )

        self.assertEqual( r.status_code, 200 )
        self.assertEqual( r.headers['Content-Type'], 'application/json' )

        data = r.json()
        self.assertEqual( data['success'], True )
        self.assertTrue( 'apartments' in data )
        self.assertTrue( 'filter' in data )

        data_apartments = data['apartments']
        self.assertGreater( len( data_apartments ), 0 )

        self.do_logout()


class ApiApartmentsCreate(ApiTestBase):

    def setUp(self):
        ApiTestBase.setUp(self)


    def test_create_before_login(self):
        url = cfg.API_BASE_URL + cfg.API_APARTMENTS
        payload = {
            "never_mind": "don't matter"
        }
        r = requests.post( url, data=payload, headers=self.headers, cookies=self.cookies )

        self.assertEqual( r.status_code, 401 )
        self.assertEqual( r.headers['Content-Type'], 'application/json' )

        data = r.json()
        self.assertEqual( data['success'], False )
        self.assertEqual( data['needs_login'], True )


    def test_create(self):

        self.do_login_client()

        url = cfg.API_BASE_URL + cfg.API_APARTMENTS
        payload = {
            "name": "a cozy apartment",
            "description": "cozy little apt.",
            "floor_area_size": 400,
            "price_per_month": 65,
            "number_of_rooms": 1,
            "geolocation_lat": "90.4",
            "geolocation_long": "120.5",
            "address": "Flyover roundabout 123, Fargo, ND 52134"
        }
        r = requests.post( url, data=json.dumps( payload ), headers=self.headers, cookies=self.cookies )

        self.assertEqual( r.status_code, 201 )
        self.assertEqual( r.headers['Content-Type'], 'application/json' )

        data = r.json()
        self.assertEqual( data['success'], True )
        self.assertTrue( 'data' in data )
        self.assertTrue( 'Location' in r.headers )

        self.do_logout()


class ApiApartmentsUpdate(ApiTestBase):

    def setUp(self):
        ApiTestBase.setUp(self)
        self.db_load_apartments()


    def test_update_before_login(self):
        url = cfg.API_BASE_URL + cfg.API_APARTMENTS
        payload = {
            "never_mind": "don't matter"
        }
        r = requests.put( url, data=payload, headers=self.headers, cookies=self.cookies )

        self.assertEqual( r.status_code, 401 )
        self.assertEqual( r.headers['Content-Type'], 'application/json' )

        data = r.json()
        self.assertEqual( data['success'], False )
        self.assertEqual( data['needs_login'], True )


    def test_update(self):

        self.do_login_client()

        url = cfg.API_BASE_URL + cfg.API_APARTMENTS
        apt_id = '100'
        payload = {
            "name": "a cozy apartment",
            "description": "cozy little apt.",
            "floor_area_size": 400,
            "price_per_month": 165,
            "number_of_rooms": 1,
            "geolocation_lat": "90.4",
            "geolocation_long": "120.5",
            "address": "Flyover roundabout 123, Fargo, ND 52134"
        }
        r = requests.put( url + '/' + apt_id, data=json.dumps( payload ), headers=self.headers, cookies=self.cookies )

        self.assertEqual( r.status_code, 200 )
        self.assertEqual( r.headers['Content-Type'], 'application/json' )

        data = r.json()
        self.assertEqual( data['success'], True )
        self.assertTrue( 'data' in data )

        self.do_logout()


class ApiApartmentsDelete(ApiTestBase):

    def setUp(self):
        ApiTestBase.setUp(self)


    def test_delete_before_login(self):
        url = cfg.API_BASE_URL + cfg.API_APARTMENTS
        r = requests.delete( url, headers=self.headers, cookies=self.cookies )

        self.assertEqual( r.status_code, 401 )
        self.assertEqual( r.headers['Content-Type'], 'application/json' )

        data = r.json()
        self.assertEqual( data['success'], False )
        self.assertEqual( data['needs_login'], True )


    def test_delete_as_client(self):

        self.db_load_apartments()
        self.do_login_client()

        url = cfg.API_BASE_URL + cfg.API_APARTMENTS
        apt_id = '100'
        r = requests.delete( url + '/' + apt_id, headers=self.headers, cookies=self.cookies )

        self.assertEqual( r.status_code, 404 )
        self.assertEqual( r.headers['Content-Type'], 'application/json' )

        data = r.json()
        self.assertEqual( data['success'], False )
        self.assertTrue( 'request' in data )

        self.do_logout()


    def test_delete(self):

        self.db_load_apartments()
        self.do_login_realtor()

        url = cfg.API_BASE_URL + cfg.API_APARTMENTS
        apt_id = '100'
        r = requests.delete( url + '/' + apt_id, headers=self.headers, cookies=self.cookies )

        self.assertEqual( r.status_code, 200 )
        self.assertEqual( r.headers['Content-Type'], 'application/json' )

        data = r.json()
        self.assertEqual( data['success'], True )
        self.assertTrue( 'request' in data )

        self.do_logout()
