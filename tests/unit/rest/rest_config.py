## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later

import os
import sys

_CFG_LOCATION = os.path.dirname( os.path.realpath( __file__ ) )

sys.path.append( os.path.normpath( '{}/../../../tools/lib'.format( _CFG_LOCATION ) ) )

APP_DB_HOST = 'localhost'
APP_DB_USER = 'dev'
APP_DB_PASS = 'devpass'
APP_DB_DATABASE = 'rentals_site'

APP_DB_CREATE_SQL_FILE = _CFG_LOCATION + '/../../../db/create.sql'
APP_DB_ION_AUTH_SQL_FILE = _CFG_LOCATION + '/../../../db/ion_auth.sql'
APP_DB_APP_TABLES_SQL_FILE = _CFG_LOCATION + '/../../../db/app_tables.sql'
APP_DB_USERS_SQL_FILE = _CFG_LOCATION + '/../../../db/users.sql'

APP_DB_TEST_USERS_SQL_FILE = _CFG_LOCATION + '/data/users.sql'
APP_DB_APARTMENTS_SQL_FILE = _CFG_LOCATION + '/data/apartments.sql'

API_BASE_URL = 'http://localhost:8080/api/index.php?'

API_AUTH_WHOAMI = '/auth/whoami'
API_AUTH_LOGIN = '/auth/login'
API_AUTH_LOGOUT = '/auth/logout'
API_AUTH_REGISTER = '/auth/register'

API_APARTMENTS = '/apartments'
API_USERS = '/users'

CLIENT_IDENTITY = 'client@arm.com'
CLIENT_PASSWORD = 'password'
CLIENT_DB_ID = '2'

OTHER_CLIENT_IDENTITY = 'other_client@arm.com'
OTHER_CLIENT_PASSWORD = 'password'
OTHER_CLIENT_DB_ID = '4'

REALTOR_IDENTITY = 'realtor@arm.com'
REALTOR_PASSWORD = 'password'
REALTOR_DB_ID = '3'

ADMIN_IDENTITY = 'admin@arm.com'
ADMIN_PASSWORD = 'password'
ADMIN_DB_ID = '1'

