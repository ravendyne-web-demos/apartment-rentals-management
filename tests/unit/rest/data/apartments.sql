
SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";



USE `rentals_site`;


TRUNCATE TABLE `apartments`;


-- APARTMENT 100 ----------

INSERT INTO `apartments` (
    `id`, `name`, `description`,
    `floor_area_size`, `price_per_month`, `number_of_rooms`,
    `geolocation_lat`, `geolocation_long`, 
    `date_added`, 
    `associated_realtor`, 
    `is_rented`, 
    `address`
) VALUES (
    100, 'the cozy apartment', 'cozy little apt.', 
    '300.000', '65.00', 1, 
    '90.40000000', '120.50000000', 
    '2019-08-06', 
    3, 
    0, 
    'Flyover roundabout 123, Fargo, ND 52134'
);



SET FOREIGN_KEY_CHECKS=1;

COMMIT;
