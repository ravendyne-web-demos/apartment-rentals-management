
/*

GROUPS

(1, 'admin', 'Administrator')
(2, 'client', 'Client User')
(3, 'realtor', 'Realtor User')

*/

USE `rentals_site`;


-- USER 100 ----------

INSERT INTO `users` (
    `id`, `ip_address`,
    `username`, `password`, `email`,
    `activation_selector`, `activation_code`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `remember_selector`, `remember_code`, `created_on`, `last_login`,
    `active`, `first_name`, `last_name`,
    `company`, `phone`
) VALUES (
    100, '127.0.0.1',
    'client_100', '$2y$12$NaT0jzTFbtYIclRifnfm4eo4l2PyU4eOM47C1qK1t1QZKS9iB2d1K', 'client_100@arm.com',
    NULL, '', NULL, NULL, NULL, '665d3d38961d6392ad39ee969868357f65e9fad0', '$2y$10$6SVKr3KMRKqBec5nj1fBUe/zEf1v.vjI8cEDr/0fKzcVwrecBw55G', 1268889823, 1560526709,
    1, 'Client_', '100',
    'CLIENT', '0'
);

INSERT INTO `users_groups`(
    `user_id`, `group_id`
) VALUES (
    100, 2
);,


-- USER 101 ----------

INSERT INTO `users` (
    `id`, `ip_address`,
    `username`, `password`, `email`,
    `activation_selector`, `activation_code`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `remember_selector`, `remember_code`, `created_on`, `last_login`,
    `active`, `first_name`, `last_name`,
    `company`, `phone`
) VALUES (
    101, '127.0.0.1',
    'client_101', '$2y$12$NaT0jzTFbtYIclRifnfm4eo4l2PyU4eOM47C1qK1t1QZKS9iB2d1K', 'client_101@arm.com',
    NULL, '', NULL, NULL, NULL, '665d3d38961d6392ad39ee969868357f65e9fad0', '$2y$10$6SVKr3KMRKqBec5nj1fBUe/zEf1v.vjI8cEDr/0fKzcVwrecBw55G', 1268889823, 1560526709,
    1, 'Client_', '101',
    'CLIENT', '0'
);

INSERT INTO `users_groups`(
    `user_id`, `group_id`
) VALUES (
    101, 2
);,




COMMIT;
