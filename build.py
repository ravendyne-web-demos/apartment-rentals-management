## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later

import build_config as cfg

from lib.helper_build import combine_files, copy_templates

def do_build():
    copy_templates( cfg.template_source, cfg.template_target )
    combine_files( cfg.source_target, cfg.source_base, cfg.sources )


if __name__ == "__main__":
    do_build()

