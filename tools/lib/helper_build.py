## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later

import shutil
import os
import fnmatch


def combine_files(dst_file, src_base, src_files):
    destination = open(dst_file, 'wb')
    destination.write('(function(){')
    for filepath in src_files:
        shutil.copyfileobj( open( src_base + filepath, 'rb'), destination )
    destination.write('})();')
    destination.close()

def copy_templates(dst_folder, src_base):
    template_files = []
    for root, folders, files in os.walk( src_base ):
        for template_file in fnmatch.filter( files, '*.html' ):
            template_file = os.path.join( root, template_file )
            template_file = os.path.relpath( template_file, src_base )
            template_files.append( template_file )
            # print src_base, ' / ', template_file

    if not os.path.exists( dst_folder ):
        os.makedirs( dst_folder )

    for template_file in template_files:
        source = os.path.join( src_base, template_file )
        target = os.path.join( dst_folder, template_file )
        target_dir = os.path.dirname( target )
        if not os.path.exists( target_dir ):
            os.makedirs( target_dir )
        # print source, '->', target
        shutil.copyfile( source, target )
