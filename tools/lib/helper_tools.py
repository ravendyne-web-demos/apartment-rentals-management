## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later


from datetime import date as ddate, time as dtime


def LocalDateString( year, month, day ):
    a_date = ddate( year, month, day )
    return a_date.strftime('%x')

def LocalTimeString( hour, minue, second ):
    a_time = dtime( hour, minue, second )
    return a_time.strftime('%X')

def ISODateString( year, month, day ):
    a_date = ddate( year, month, day )
    return a_date.strftime('%Y-%m-%d')

def ISOTimeString( hour, minue, second ):
    a_time = dtime( hour, minue, second )
    return a_time.strftime('%H:%M:%S')


def _do_bunch_of_sql( cursor, sql_stmts ):
    sql_cmds = sql_stmts.split(';')
    for one_sql_cmd in sql_cmds:
        try:
            if one_sql_cmd.strip() != '':
                cursor.execute(one_sql_cmd)
        except IOError, msg:
            print "Error executing a command SQL: ", msg


def RunSQLFile( conn, file_path ):

    sql_stmts = ''

    with open(file_path) as f:
        sql_stmts = f.read()

    dbcursor = conn.cursor()

    sql_cmds = sql_stmts.split(';')

    for one_sql_cmd in sql_cmds:

        if one_sql_cmd.strip() != '':
            # print one_sql_cmd
            dbcursor.execute( one_sql_cmd )

    dbcursor.close()
