## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later

from smtpd import SMTPServer
import asyncore
from threading import Thread

class DummySmtpd(SMTPServer):
    def __init__(self):
        # super(SMTPServer, self).__init__(('localhost', 25000), ())
        SMTPServer.__init__(self, ('localhost', 25000), ())

    def process_message(self, peer, mailfrom, rcpttos, data):
        # just discard whatever comes in
        pass

class UnitTestSmtpServer(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.smtp_server = DummySmtpd()
    
    def run(self):
        asyncore.loop()
    
    def stop(self):
        self.smtp_server.close()


def start_test_server():
    eml_srvr = UnitTestSmtpServer()
    eml_srvr.start()
    raw_input('Press Enter to stop server...')
    print 'Stopping eml_srvr...'
    eml_srvr.stop()
    eml_srvr.join()
    print 'Done.'

if __name__ == "__main__":
    start_test_server()
