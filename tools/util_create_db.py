## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later

import mysql.connector

from lib.helper_tools import RunSQLFile
import tools_config as cfg


def util_create_db():

    conn = mysql.connector.connect(
        # database = cfg.APP_DB_DATABASE,
        host = cfg.APP_DB_HOST,
        user = cfg.APP_DB_USER,
        passwd = cfg.APP_DB_PASS
    )

    RunSQLFile( conn, cfg.APP_DB_CREATE_SQL_FILE )
    RunSQLFile( conn, cfg.APP_DB_ION_AUTH_SQL_FILE )
    RunSQLFile( conn, cfg.APP_DB_APP_TABLES_SQL_FILE )
    RunSQLFile( conn, cfg.APP_DB_USERS_SQL_FILE )

    conn.close()

if __name__ == "__main__":
    util_create_db()
