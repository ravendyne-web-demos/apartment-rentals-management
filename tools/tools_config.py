## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later

import os

_CFG_LOCATION = os.path.dirname( os.path.realpath( __file__ ) )

APP_DB_HOST = 'localhost'
APP_DB_USER = 'dev'
APP_DB_PASS = 'devpass'
APP_DB_DATABASE = 'rentals_site'

APP_DB_CREATE_SQL_FILE = _CFG_LOCATION + '/../db/create.sql'
APP_DB_ION_AUTH_SQL_FILE = _CFG_LOCATION + '/../db/ion_auth.sql'
APP_DB_APP_TABLES_SQL_FILE = _CFG_LOCATION + '/../db/app_tables.sql'
APP_DB_USERS_SQL_FILE = _CFG_LOCATION + '/../db/users.sql'

APP_DB_SAMPLE_DATA_SQL_FILE = _CFG_LOCATION + '/../db/sample_data.sql'
