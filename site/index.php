<?php?>
<!DOCTYPE html>
<html ng-app="armApp">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Apartment Rental Management</title>

        <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> -->
        <link rel="stylesheet" href="./css/bootstrap.min.css">
        <link rel="stylesheet" href="./css/fontawesome.min.css">
        <link rel="stylesheet" href="./css/brands.min.css">
        <link rel="stylesheet" href="./css/solid.min.css">
        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css" /> -->
        <link rel="stylesheet" href="./css/tempusdominus-bootstrap-4.min.css" />
        <link rel="stylesheet" href="./css/site.css">
        <!-- <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/> -->
        <link rel="stylesheet" href="./css/leaflet.css">


        <!-- <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha384-tsQFqpEReu7ZLhBV2VZlAu7zcOV+rXbYlF2cqB8txI/8aZajjp4Bqd+V6D5IgvKT" crossorigin="anonymous"></script> -->
        <script src="./js/jquery-3.3.1.min.js"></script>
        <script src="./js/angular-1.7.8.min.js"></script>
        <script src="./js/angular-route-1.7.8.min.js"></script>
        <!-- Make sure you put this AFTER Leaflet's CSS -->
        <!-- <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js" integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og==" crossorigin=""></script> -->
        <script src="./js/leaflet.js"></script>

    </head>
    <body>
    <main role="main">
        <div class="container">

        <div class="row" ng-if="$root.user_data">
            <div class="col-12">
            <arm-top-card></arm-top-card>
            </div>
        </div> <!-- row -->

        <div class="row">
            <div class="col-12">
            <div ng-view></div>
            </div>
        </div> <!-- row -->

        <div class="row" ng-if="$root.user_data">
            <div class="col-12">
            <arm-bottom-card></arm-bottom-card>
            </div>
        </div> <!-- row -->



        </div> <!-- containter -->
    </main>

    <?php include 'modal.html'; ?>

    </body>

    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script> -->
    <script src="./js/popper.min.js"></script>
    <script src="./js/bootstrap.min.js"></script>

    <!-- <script src="./js/moment-with-locales.min.js"></script> -->
    <!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script> -->
    <script type="text/javascript" src="./js//moment.min.js"></script>
    <script type="text/javascript" src="./js/tempusdominus-bootstrap-4.min.js"></script>

    <script src="./js/arm-app.js"></script>

    <script>
        $(document).ready(function(){
            // ARM_on_inital_app_load();
        })
    </script>
</html>
