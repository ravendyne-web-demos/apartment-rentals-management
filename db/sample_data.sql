
SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";



USE `rentals_site`;


TRUNCATE TABLE `apartments`;


-- REALTOR 3 ----------
-- APARTMENT 300 ----------

INSERT INTO `apartments` (
    `id`, `name`, `description`,
    `floor_area_size`, `price_per_month`, `number_of_rooms`,
    `geolocation_lat`, `geolocation_long`, 
    `date_added`, 
    `associated_realtor`, 
    `is_rented`, 
    `address`
) VALUES (
    300, 'the cozy apartment 3', 'cozy little apt.', 
    '300.000', '65.00', 1, 
    '43.6692', '-79.3939',
    '2019-08-06', 
    3, 
    0, 
    'Flyover roundabout 123, Fargo, ND 52134'
);

-- APARTMENT 301 ----------

INSERT INTO `apartments` (
    `id`, `name`, `description`,
    `floor_area_size`, `price_per_month`, `number_of_rooms`,
    `geolocation_lat`, `geolocation_long`, 
    `date_added`, 
    `associated_realtor`, 
    `is_rented`, 
    `address`
) VALUES (
    301, 'rented apartment 3', 'this apt. is RENTED', 
    '1300.000', '165.00', 4, 
    '43.6680', '-79.4112',
    '2019-08-06', 
    3, 
    1, 
    'Flyover roundabout 123, Fargo, ND 52134'
);

-- REALTOR 5 ----------
-- APARTMENT 500 ----------

INSERT INTO `apartments` (
    `id`, `name`, `description`,
    `floor_area_size`, `price_per_month`, `number_of_rooms`,
    `geolocation_lat`, `geolocation_long`, 
    `date_added`, 
    `associated_realtor`, 
    `is_rented`, 
    `address`
) VALUES (
    500, 'the cozy apartment 5', 'cozy little apt.', 
    '300.000', '65.00', 1, 
    '43.6615', '-79.3982',
    '2019-08-06', 
    5, 
    0, 
    'Flyover roundabout 123, Fargo, ND 52134'
);

-- APARTMENT 501 ----------

INSERT INTO `apartments` (
    `id`, `name`, `description`,
    `floor_area_size`, `price_per_month`, `number_of_rooms`,
    `geolocation_lat`, `geolocation_long`, 
    `date_added`, 
    `associated_realtor`, 
    `is_rented`, 
    `address`
) VALUES (
    501, 'rented apartment 5', 'this apt. is RENTED', 
    '1300.000', '165.00', 4, 
    '43.6547', '-79.3920',
    '2019-08-06', 
    5, 
    1, 
    'Flyover roundabout 123, Fargo, ND 52134'
);



SET FOREIGN_KEY_CHECKS=1;

COMMIT;
