
SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rentals_site`
--
USE `rentals_site`;

-- --------------------------------------------------------

--
-- Table structure for table `apartment`
--

DROP TABLE IF EXISTS `apartments`;
CREATE TABLE `apartments` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` text NOT NULL,
  `description` text,
  `floor_area_size` decimal(10,3) NOT NULL,
  `price_per_month` decimal(10,2) NOT NULL,
  `number_of_rooms` int(10) UNSIGNED NOT NULL,
  `geolocation_lat` decimal(10,8) DEFAULT NULL,
  `geolocation_long` decimal(11,8) DEFAULT NULL,
  `date_added` date NOT NULL,
  `associated_realtor` int(11) UNSIGNED NOT NULL,
  `is_rented` tinyint(1) NOT NULL DEFAULT '0',
  `address` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for table `apartment`
--
ALTER TABLE `apartments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `associated_realtor` (`associated_realtor`);

--
-- AUTO_INCREMENT for table `apartment`
--
ALTER TABLE `apartments`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for table `apartments`
--
ALTER TABLE `apartments`
  ADD CONSTRAINT `apartment_realtor_fk` FOREIGN KEY (`associated_realtor`) REFERENCES `users` (`id`);

-- --------------------------------------------------------


SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
