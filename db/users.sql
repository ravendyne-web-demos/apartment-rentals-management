
SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `rentals_site`
--
USE `rentals_site`;

--
-- Truncate table before insert `users`
--

TRUNCATE TABLE `users`;
--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `email`, `activation_selector`, `activation_code`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `remember_selector`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES(1, '127.0.0.1', 'administrator', '$2y$12$NaT0jzTFbtYIclRifnfm4eo4l2PyU4eOM47C1qK1t1QZKS9iB2d1K', 'admin@arm.com', NULL, '', NULL, NULL, NULL, '665d3d38961d6392ad39ee969868357f65e9fad0', '$2y$10$6SVKr3KMRKqBec5nj1fBUe/zEf1v.vjI8cEDr/0fKzcVwrecBw55G', 1268889823, 1560526709, 1, 'Admin', 'istrator', 'ADMIN', '0');
INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `email`, `activation_selector`, `activation_code`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `remember_selector`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES(2, '10.0.2.2', 'client_user', '$2y$10$GwgcqEwJj8cJKgjpv6K9MuAw2IFatTFCZIx3JOra1rFZf66ffQ3l.', 'client@arm.com', NULL, NULL, NULL, NULL, NULL, '9795e9e6c00b1fb58a6c51aba8fd8be4d0d28f5c', '$2y$10$.A4ZSfOpUnFKFC7BkQRmO.mxTmkMBg.1wNAmGEZY8Pkge3tBrxs.y', 1560527510, 1560527740, 1, '', '', NULL, NULL);
INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `email`, `activation_selector`, `activation_code`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `remember_selector`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES(3, '10.0.2.2', 'realtor', '$2y$10$lmi8AEQln5smMrPxKY6hjOkb4pxA7fUS31dJLJ1nbH8JQKzf1AMm2', 'realtor@arm.com', NULL, NULL, NULL, NULL, NULL, '21e30d980f5ba303404097b145d7f35e056838b6', '$2y$10$L05qNKhp9dH7k1.xXD58V.uoEcLqCuqW5upx33Gz6.xLxKUUewVpC', 1560527549, 1560527698, 1, '', '', NULL, NULL);
INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `email`, `activation_selector`, `activation_code`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `remember_selector`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES(4, '10.0.2.2', 'another_client_user', '$2y$10$GwgcqEwJj8cJKgjpv6K9MuAw2IFatTFCZIx3JOra1rFZf66ffQ3l.', 'other_client@arm.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1560717271, NULL, 1, '', '', NULL, NULL);
INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `email`, `activation_selector`, `activation_code`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `remember_selector`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES(5, '10.0.2.2', 'another_realtor', '$2y$10$lmi8AEQln5smMrPxKY6hjOkb4pxA7fUS31dJLJ1nbH8JQKzf1AMm2', 'other_realtor@arm.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1560527549, 1560527698, 1, '', '', NULL, NULL);

--
-- Truncate table before insert `groups`
--

TRUNCATE TABLE `groups`;
--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'client', 'Client User'),
(3, 'realtor', 'Realtor User');


--
-- Truncate table before insert `users_groups`
--

TRUNCATE TABLE `users_groups`;
--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3),
(4, 4, 2),
(5, 5, 3);

SET FOREIGN_KEY_CHECKS=1;

COMMIT;
