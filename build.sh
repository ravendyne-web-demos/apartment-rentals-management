#!/bin/bash

rsync -avu api/ build/api --delete
rsync -avu site/ build/site --delete
# rsync -avu jssrc/app/ build/site/js/app --delete
python build.py
