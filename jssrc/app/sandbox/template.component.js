/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

armTemplate = angular.module('armTemplate')

armTemplate.component('armTemplate', {

    // template: 'Hello, {{$ctrl.data_point}}!',
    templateUrl: 'js/app/[path-to-component-template]/[name].template.html',

    controller: function GreetUserController() {
        this.data_point = 'world';
    }
});
