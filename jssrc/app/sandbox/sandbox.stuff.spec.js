/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

describe('armSandbox', function() {

    beforeEach(module('armSandbox'));

    it('should create a `apartments` model with 3 apartments', inject(function($controller) {
        var scope = {};
        var ctrl = $controller('armController', {$scope: scope});

        expect(scope.apartments.length).toBe(3);
    }));

});
