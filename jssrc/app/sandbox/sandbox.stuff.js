/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

armSandbox = angular.module('armSandbox')


// Define the `armController` controller on the `armSandbox` module
armSandbox.controller('armController', function ($scope) {
    $scope.apartments = [
        {
            name: 'Nexus S',
            description: 'Fast just got faster with Nexus S.'
        }, {
            name: 'Motorola XOOM™ with Wi-Fi',
            description: 'The Next, Next Generation tablet.'
        }, {
            name: 'MOTOROLA XOOM™',
            description: 'The Next, Next Generation tablet.'
        }
    ];
});

armSandbox.component('greetUser', {

    template: 'Hello, {{$ctrl.user}}!',

    controller: function GreetUserController() {
        this.user = 'world';
    }
});
