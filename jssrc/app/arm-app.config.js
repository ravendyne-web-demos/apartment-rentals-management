/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

angular
.module('armApp')
.config(['$routeProvider',
    function config($routeProvider) {
        $routeProvider.
        when('/dash', {
            template: '<arm-dash></arm-dash>'
        }).
        when('/login', {
            template: '<arm-app-login></arm-app-login>'
        }).
        when('/oauth2/:provider', {
            template: '<arm-oauth2-login></arm-oauth2-login>'
        }).
        when('/register/:email?', {
            template: '<arm-app-register></arm-app-register>'
        }).
        when('/registration-success/:noConfirmation?', {
            template: '<arm-app-registration-success></arm-app-registration-success>'
        }).

        when('/admin-dash', {
            template: '<arm-admin-dash></arm-admin-dash>'
        }).
        when('/users-list', {
            template: '<arm-users-list></arm-users-list>'
        }).
        when('/user-detail/:userId', {
            template: '<arm-user-detail></arm-user-detail>'
        }).

        when('/apartment-list', {
            template: '<arm-apartment-list></arm-apartment-list>'
        }).
        when('/apartment-detail/:apartmentId', {
            template: '<arm-apartment-detail></arm-apartment-detail>'
        }).
        when('/apartment-view/:apartmentId', {
            template: '<arm-apartment-view></arm-apartment-view>'
        }).

        when('/message/:messageText', {
            template: '<arm-flash-message></arm-flash-message>'
        }).

        otherwise('/dash');
    }
]);
