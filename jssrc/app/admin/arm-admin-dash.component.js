/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

angular
.module('armAdmin')
.component('armAdminDash', {

    templateUrl: 'js/app/admin/admin-dash.template.html',
    controller: function AdminDashController() {
    }
});
