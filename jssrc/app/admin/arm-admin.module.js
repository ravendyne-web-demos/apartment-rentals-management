/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

// Define the `armAdmin` module
angular.module('armAdmin', [
    'ngRoute'
]);
