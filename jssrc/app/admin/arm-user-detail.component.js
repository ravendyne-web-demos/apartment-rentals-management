/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

angular
.module('armAdmin')
.component('armUserDetail', {

    templateUrl: 'js/app/admin/user-detail.template.html',
    controller: ['$location', '$routeParams', 'armUsersService', 'armSystemService',
    function UserDetailController($location, $routeParams, armUsersService, armSystemService) {
        var self = this
        var userId = $routeParams.userId

        self.update_mode = false
        if( userId && userId > 0 ) {
            self.update_mode = true
        }
        self.user = {}
        self.new_user_password = ''
        self.user_group = ''

        let setGroupAndPassword = function(user_data) {

            if( self.user_group == 'realtor' ) {
                user_data.realtor = true
            } else {
                user_data.realtor = false
            }

            if( self.new_user_password ) {
                user_data.password = self.new_user_password
                self.new_user_password = ''
            }
        }

        this.clearUser = function() {
            self.user = {
                associated_realtor: self.all_realtors[0],
                is_rented: "0",
            }
        }

        this.addUser = function() {

            let user_data = self.user
            // console.log('updateUser: user_data', user_data)

            user_data.id = undefined
            user_data.is_admin = undefined

            setGroupAndPassword(user_data)
            // console.log('updateUser: user_data 2', user_data)

            armUsersService
            .createUser( user_data )
            .then(function(response) {
                // console.log('create-user', response)
                // go to user list
                $location.path('/user-list')
            })
            .catch(function(error) {
                msg = 'Error while creating'
                if( error.status == 404 ) {
                    msg = 'Nothing created'
                }
                armSystemService.showMessage(msg)
            })
        }

        this.updateUser = function() {

            let user_data = self.user
            // console.log('updateUser: user_data', user_data)

            setGroupAndPassword(user_data)
            // console.log('updateUser: user_data 2', user_data)

            armUsersService
            .updateUser( userId, user_data )
            .then(function(response) {
                // console.log('update-user', response)
                // go to user list
                $location.path('/user-list')
            })
            .catch(function(error) {
                msg = 'Error while updating'
                if( error.status == 404 ) {
                    msg = 'Nothing updated'
                }
                armSystemService.showMessage(msg)
            })
        }

        if( self.update_mode ) {

            armUsersService
            .getUser( userId )
            .then(function(response) {
                // console.log(response)
                self.user = response
                self.user_group = self.user.groups[0]
                // console.log( 'self.user', self.user )
                // console.log( 'self.user_group', self.user_group )
            })

        } else {

            self.user = {}
        }
    }]
});
