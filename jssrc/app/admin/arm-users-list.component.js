/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

angular
.module('armAdmin')
.component('armUsersList', {

    templateUrl: 'js/app/admin/users-list.template.html',
    controller: [ '$location', 'armUsersService', 'armSystemService',
    function AdminDashController( $location, armUsersService, armSystemService ) {
        let self = this
        self.users = []
        self.invite_email = ''

        this.inviteUser = function() {

            if( ! self.invite_email ) {
                armSystemService.showMessage('Email is required.')
                return
            }

            armUsersService
            .sendInvitation( self.invite_email )
            .then(function(result) {
                armSystemService.showMessage('Invitation to "' + self.invite_email + '" sent.')
                self.invite_email = ''
            })
            .catch(function(error) {
                armSystemService.showMessage('Error while trying to send invitation')
                console.error(error)
            })
        }

        this.newUser = function() {
            $location.path('/user-detail/0')
        }

        this.showUser = function( userId ) {
            $location.path( '/user-detail/' + userId )
        }

        this.deactivateUser = function( userId ) {
            if( ! confirm( 'Deactivate selected user?' ) ) {
                return
            }

            armUsersService
            .deactivateUser( userId )
            .then(function(response) {
                loadUsers()
            })
            .catch(function(error) {
                msg = 'Error while trying to deactivate this user'
                armSystemService.showMessage(msg)
            })
        }

        let loadUsers = function() {
            armUsersService
            .getAllUsers()
            .then(function(result) {
                self.users = result
            })
            .catch(function(error) {
                armSystemService.showMessage('Error while fetching users')
                // console.error(error)
            })
        }

        loadUsers()

    }]
});
