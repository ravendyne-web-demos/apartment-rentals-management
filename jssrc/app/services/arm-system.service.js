/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

angular
.module('armServices')
.factory('armSystemService', function($http, $q, $location) {

    let _showMessage = function( message ) {
        $('#system-modal #message').html(message);
        $('#system-modal').modal('show');
        console.log( message );
    }

    return {
        showMessage: _showMessage,
    }
});
