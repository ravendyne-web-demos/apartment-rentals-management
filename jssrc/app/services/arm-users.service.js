/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

angular
.module('armServices')
.factory('armUsersService', function($http, $q, $location) {

    var _allRealtors = undefined

    let _getUser = function( userId ) {

        return $q( function(resolve, reject) {

            $http.get(
            '/api/index.php?/users/' + userId, {
                headers: {
                    'Accept': 'application/json',
                    // 'Content-Type': 'application/json'
                }
            })
            .then(function(response) {
                // console.log(response)
                if( response.status == 200 ) {

                    resolve( response.data.user )

                } else {
                    reject( response )
                }
            })
            .catch(function(error) {
                if( error.status == 401 ) {
                    // console.error( 'needs login' )
                    $locatio.path('/login')
                    return
                }
                console.error( 'error get user', error )
                reject( error )
            })
        })
    }

    let _getAllUsers = function() {

        return $q( function(resolve, reject) {

            $http.get(
            '/api/index.php?/users', {
                headers: {
                    'Accept': 'application/json',
                    // 'Content-Type': 'application/json'
                }
            })
            .then(function(response) {
                // console.log(response)
                if( response.status == 200 ) {

                    resolve( response.data.users )

                } else {
                    reject( response )
                }
            })
            .catch(function(error) {
                if( error.status == 401 ) {
                    // console.error( 'needs login' )
                    $locatio.path('/login')
                    return
                }
                console.error( 'error get all users', error )
                reject( error )
            })
        })
    }

    let _getAllRealtors = function() {

        return $q( function(resolve, reject) {

            if( _allRealtors ) {
                resolve( _allRealtors )
                return
            }

            $http.get(
            '/api/index.php?/users/realtors', {
                headers: {
                    'Accept': 'application/json',
                    // 'Content-Type': 'application/json'
                }
            })
            .then(function(response) {
                // console.log(response)
                if( response.status == 200 ) {

                    _allRealtors = response.data.users
                    resolve( _allRealtors )

                } else {
                    reject( response )
                }
            })
            .catch(function(error) {
                if( error.status == 401 ) {
                    // console.error( 'needs login' )
                    $locatio.path('/login')
                    return
                }
                console.error( 'error get all realtors', error )
                reject( error )
            })
        })
    }

    let _updateUser = function( userId, user_data ) {

        return $q( function(resolve, reject) {

            $http.put(
            '/api/index.php?/users/' + userId,
            user_data, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then(function(response) {
                console.log('update user', response)
                if( response.status == 200 ) {
                    if( response.data.success ) {

                        resolve( true )

                    } else {
                        console.error('error', response)
                        reject( response )
                    }
                }
            })
            .catch(function(error) {

                if( error.status == 401 ) {
                    // console.error( 'needs login' )
                    $locatio.path('/login')
                    return
                }
                console.error( 'error update user', error )
                reject( error )
            })
        })
    }

    let _createUser = function( user_data ) {

        return $q( function(resolve, reject) {

            $http.post(
            '/api/index.php?/users',
            user_data, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then(function(response) {
                console.log('create user', response)
                if( response.status == 201 ) {
                    if( response.data.success ) {

                        resolve( true )

                    } else {
                        console.error('error', response)
                        reject( response )
                    }
                }
            })
            .catch(function(error) {

                if( error.status == 401 ) {
                    // console.error( 'needs login' )
                    $locatio.path('/login')
                    return
                }
                console.error( 'error create user', error )
                reject( error )
            })
        })
    }

    let _sendInvitation = function( email ) {

        return $q( function(resolve, reject) {

            $http.get(
            '/api/index.php?/users/send_invitation/' + encodeURIComponent( email ), {
                headers: {
                    'Accept': 'application/json',
                    // 'Content-Type': 'application/json'
                }
            })
            .then(function(response) {
                console.log('invite user', response)
                if( response.status == 200 ) {
                    if( response.data.success ) {

                        resolve( true )

                    } else {
                        console.error('error', response)
                        reject( response )
                    }
                }
            })
            .catch(function(error) {

                if( error.status == 401 ) {
                    // console.error( 'needs login' )
                    $locatio.path('/login')
                    return
                }
                console.error( 'error send invite', error )
                reject( error )
            })
        })
    }

    let _deactivateUser = function( userId ) {

        return $q( function(resolve, reject) {

            $http.delete(
            '/api/index.php?/users/' + userId, {
                headers: {
                    'Accept': 'application/json',
                    // 'Content-Type': 'application/json'
                }
            })
            .then(function(response) {
                console.log('deactivate user', response)

                if( response.status == 200 ) {
                    if( response.data.success ) {

                        resolve( response.data )

                    } else {

                        console.error('error', response)
                        reject( response )
                    }
                }
            })
            .catch(function(error) {

                if( error.status == 401 ) {
                    // console.error( 'needs login' )
                    $locatio.path('/login')
                    return
                }
                console.error( 'error deactivating user', error )
                reject( error )
            })
        })
    }

    return {
        getUser: _getUser,
        getAllUsers: _getAllUsers,
        getAllRealtors: _getAllRealtors,
        updateUser: _updateUser,
        createUser: _createUser,
        sendInvitation: _sendInvitation,
        deactivateUser: _deactivateUser,
    }
});
