/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

angular
.module('armServices')
.factory('armAuthService', function($http, $q, $location) {

    var __user_data = undefined

    let _augmentUserData = function( user_data ) {

        user_data.is_client = false
        user_data.is_realtor = false

        if( $.inArray( 'realtor', user_data.groups ) > -1 ) {
            user_data.is_realtor = true

        } else if( $.inArray( 'client', user_data.groups ) > -1 ) {
            user_data.is_client = true
        }
    }

    let _whoAmI = function () {

        return $q( function(resolve, reject) {

            if( __user_data ) {
                resolve( __user_data )
                return
            }

            $http.get(
            '/api/index.php?/auth/whoami', {
                headers: {
                    'Accept': 'application/json',
                    // 'Content-Type': 'application/json'
                }
            })
            .then(function(response) {
                // console.log(response)
                if( response.status == 200 ) {
                    if( response.data.success ) {

                        __user_data = response.data.user
                        _augmentUserData( __user_data )
                        resolve( __user_data )

                    } else {

                        console.error('no whoami', response.data)
                        reject( response )
                    }
                }
            })
            .catch(function(error) {
                if( error.status == 401 ) {
                    $location.path('/login')
                    return
                }
                reject( error )
            })
        })
    }

    let _login = function( payload ) {

        return $q( function(resolve, reject) {
            $http.post(
            '/api/index.php?/auth/login',
            payload, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then(function(response) {
                // console.log(response)
                if( response.status == 200 ) {
                    if( response.data.success ) {

                        // console.log( '_login', response.data )
                        resolve( response.data )

                    } else {
                        console.error('no login', response.data)
                        reject( response )
                    }
                }
            })
            .catch(function(error) {

                console.error( 'error response', error )
                if( error.status == 401 ) {
                    console.error( 'login error' )
                    // show error
                }
                reject( error )
            })
        })
    }

    let _oauth2_login = function( provider ) {

        // console.log( 'document.location', document.location )
        document.location.assign( document.location.origin + '/api/index.php?/auth/oauth2/' + provider )
    }

    let _logout = function() {

        return $q( function(resolve, reject) {

            $http.post(
            '/api/index.php?/auth/logout', {
                headers: {
                    'Accept': 'application/json',
                    // 'Content-Type': 'application/json'
                }
            })
            .then(function(response) {
                if( response.status == 200 ) {
                    if( response.data.success ) {

                        __user_data = undefined
                        resolve( true )

                    } else {
                        // console.error('logout error', response.data)
                        reject( response )
                    }
                }
            })
            .catch(function(error) {
                // console.error( 'error response', error )
                reject( error )
            })
        })
    }

    let _register = function( user_data ) {

        return $q( function(resolve, reject) {

            $http.post(
            '/api/index.php?/auth/register',
            user_data, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then(function(response) {
                if( response.status == 200 ) {
                    if( response.data.success ) {

                        resolve( true )

                    } else {
                        // console.error('registration error', response.data)
                        reject( response )
                    }
                }
            })
            .catch(function(error) {
                // console.error( 'error response', error )
                reject( error )
            })
        })
    }

    return {
        whoAmI: _whoAmI,
        login: _login,
        oauth2_login: _oauth2_login,
        logout: _logout,
        register: _register,
    }
});
