/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

angular
.module('armServices')
.factory('armApartmentsService', function($http, $q, $location) {

    let _paramSerializer = function( data ) {
        var result = ''
        angular.forEach( data, function( value, key ) {
            if( value ) {
                result += '/' + key + '/' + value
            }
        })
        return result
    }

    let _getApartment = function( apartmentId ) {

        return $q( function(resolve, reject) {
            $http.get(
            '/api/index.php?/apartments/' + apartmentId, {
                headers: {
                    'Accept': 'application/json',
                    // 'Content-Type': 'application/json'
                }
            })
            .then(function(response) {
                // console.log('_getApartment',response)
                if( response.status == 200 ) {
                    resolve( response.data.apartments[0] )
                } else {
                    reject( response )
                }
            })
            .catch(function(error) {
                if( error.status == 401 ) {
                    // console.error( 'needs login' )
                    $locatio.path('/login')
                    return
                }
                console.error( 'error get apt', error )
                reject( error )
            })
        })
    }

    let _getAllApartments = function( filter ) {

        return $q( function(resolve, reject) {

            var params = ''
            if( filter ) {
                params = '/filtered' + _paramSerializer( filter )
            }
            // console.log('params', params)

            $http.get(
            '/api/index.php?/apartments' + params, {
                headers: {
                    'Accept': 'application/json',
                    // 'Content-Type': 'application/json'
                }
            })
            .then(function(response) {
                // console.log(response)
                if( response.status == 200 ) {
                    resolve( response.data.apartments )
                } else {
                    reject( response )
                }
            })
            .catch(function(error) {
                if( error.status == 401 ) {
                    // console.error( 'needs login' )
                    $locatio.path('/login')
                    return
                }
                console.error( 'error get all apt', error )
                reject( error )
            })
        })
    }

    let _updateApartment = function( apartmentId, apartment_data ) {

        return $q( function(resolve, reject) {

            $http.put(
            '/api/index.php?/apartments/' + apartmentId,
            apartment_data, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then(function(response) {
                // console.log('update apartment', response)
                if( response.status == 200 ) {
                    if( response.data.success ) {

                        resolve( response.data )

                    } else {
                        console.error('error', response)
                        reject( response )
                    }
                }
            })
            .catch(function(error) {
                if( error.status == 401 ) {
                    // console.error( 'needs login' )
                    $locatio.path('/login')
                    return
                }
                console.error( 'error updating apt', error )
                reject( error )
            })
        })
    }

    let _createApartment = function( apartment_data ) {

        return $q( function(resolve, reject) {

            $http.post(
            '/api/index.php?/apartments',
            apartment_data, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then(function(response) {
                // console.log('create apartment', response)
                if( response.status == 201 ) {
                    if( response.data.success ) {

                        resolve( true )

                    } else {
                        console.error('error', response)
                        reject( response )
                    }
                }
            })
            .catch(function(error) {
                if( error.status == 401 ) {
                    // console.error( 'needs login' )
                    $locatio.path('/login')
                    return
                }
                console.error( 'error creating apt', error )
                reject( error )
            })
        })
    }

    let _deleteApartment = function( apartmentId ) {

        return $q( function(resolve, reject) {

            $http.delete(
            '/api/index.php?/apartments/' + apartmentId, {
                headers: {
                    'Accept': 'application/json',
                    // 'Content-Type': 'application/json'
                }
            })
            .then(function(response) {
                // console.log('delete apartment', response)
                if( response.status == 200 ) {
                    if( response.data.success ) {

                        resolve( response.data )

                    } else {
                        console.error('error', response)
                        reject( response )
                    }
                }
            })
            .catch(function(error) {
                if( error.status == 401 ) {
                    // console.error( 'needs login' )
                    $locatio.path('/login')
                    return
                }
                console.error( 'error deleting apt', error )
                reject( error )
            })
        })
    }

    return {
        getApartment: _getApartment,
        getAllApartments: _getAllApartments,
        updateApartment: _updateApartment,
        createApartment: _createApartment,
        deleteApartment: _deleteApartment,
    }
});
