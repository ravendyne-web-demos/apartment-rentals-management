/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

angular
.module('armCommonModule')
.component('armBottomCard', {

    templateUrl: 'js/app/common/bottom-card.template.html',

    controller: function BottomCardController() {
    }
});
