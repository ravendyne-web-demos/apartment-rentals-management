/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

angular
.module('armCommonModule')
.component('armFlashMessage', {

    templateUrl: 'js/app/common/flash-message.template.html',

    controller: [ '$routeParams', function($routeParams) {
        try {
            this.messageText = decodeURIComponent( $routeParams.messageText.replace(/\+/g, ' ') )
        } catch(e) {
            this.messageText = 'Invalid message text'
        }
    }]
});
