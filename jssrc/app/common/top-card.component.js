/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

angular
.module('armCommonModule')
.component('armTopCard', {

    templateUrl: 'js/app/common/top-card.template.html',

    controller: [ '$location', '$rootScope', 'armAuthService', function TopCardController($location, $rootScope, armAuthService) {
        var self = this

        this.doLogout = function() {

            armAuthService.logout()
            .then(function(response) {
                $rootScope.user_data = undefined
                // navigate to login page
                $location.path('/login')
            })
            .catch(function(error) {
                console.error( 'logout error response', error )
            })
        }
    }]
});
