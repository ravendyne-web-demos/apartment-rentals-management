/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

angular
.module('armCommonModule')
.directive('armMap', ['$rootScope', function( $rootScope ) {
    return {
        restrict: 'E',
        templateUrl: 'js/app/common/arm-map.template.html',
        scope: {
            mapMode: '@mode',
            singlePoi: '=',
            poiList: '=',
            onMapClick: '<',
        },
        link: function( scope, element, attrs ) {
            var self = this
            let defaultZoom = 13

            self.mode = scope.mapMode
            // console.log('arm map ctrlr runs as: ', self.mode)

            var poiMap = undefined

            let createMap = function( pois ) {

                poiMap = L.map('arm-map')
                if( Array.isArray( pois ) ) {

                    if( pois.length > 1 ) {

                        var bounds = L.latLngBounds( pois ).pad( 0.15 )

                        poiMap.fitBounds( bounds )
                        // poiMap.fitBounds( pois )

                    } else {

                        poiMap.setView( pois[0], defaultZoom )
                    }
                }

                L.tileLayer(
                    'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png?{foo}', {
                        foo: 'bar', 
                        maxZoom: 18,
                        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>'
                        })
                .addTo( poiMap );
            }

            let addPoi = function( poi ) {

                var marker = L.marker( poi ).addTo( poiMap );
                if( poi.name ) {
                    // TODO use $sanitize?
                    marker.bindPopup( '<b>' + poi.name + '</b>' ).openPopup();
                }
                return marker
            }

            if( self.mode == 'single' ) {

                scope.$watch( 'singlePoi', function( poi ) {

                    var poiMarker = undefined

                    // console.log('scope.$watch( attrs.singlePoi', attrs.singlePoi, poi )
                    if( poi && poi.lat && poi.lon ) {

                        if( poiMap === undefined ) {
                            createMap( [ poi ] )

                            let mapClickHandler = function( e ) {
                                // console.log("You clicked the map at " + e.latlng);
                                poi.lat = e.latlng.lat
                                poi.lon = e.latlng.lng
                                if( poiMarker ) {
                                    poiMarker.setLatLng( poi )
                                }
                                if( scope.onMapClick ) {
                                    scope.onMapClick()
                                }
                            }
    
                            if( $rootScope.user_data && !$rootScope.user_data.is_client ) {
                                poiMap.on( 'click', mapClickHandler );
                            }
                        }

                        if( poiMarker ) {
                            poiMarker.setLatLng( poi )
                        } else {
                            poiMarker = addPoi( poi )
                        }
                        poiMap.setView( poi, defaultZoom )
                    }
                });
    
            } else if( self.mode == 'many' ) {

                var poiMarkerList = []

                scope.$watch( 'poiList', function( pois ) {

                    for( var idx in poiMarkerList ) {
                        poiMarkerList[ idx ].removeFrom( poiMap )
                    }
                    poiMarkerList = []

                    // console.log('scope.$watch( poiList', attrs.poiList, pois )
                    if( pois && pois.length > 0 ) {

                        if( poiMap === undefined ) {
                            createMap( pois )
                        }

                        for( var idx in pois ) {
                            var aPoi = addPoi( pois[ idx ] )
                            poiMarkerList.push( aPoi )
                        }
                    }
                });
            }
        }
    }
}]);
