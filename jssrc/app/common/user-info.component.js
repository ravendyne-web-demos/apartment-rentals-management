/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

angular
.module('armCommonModule')
.component('armUserInfo', {

    templateUrl: 'js/app/common/user-info.template.html',

    controller: [ '$rootScope', '$scope', '$http', 'armSystemService', function( $rootScope, $scope, $http, armSystemService ) {
        var self = this
        let maxUploadSize = 300000 // 300k

        $scope.uploadPhoto = function( files ) {
            // console.log('files', files)
            var avatar = files[ 0 ]

            if( avatar.size > maxUploadSize ) {
                armSystemService.showMessage('Picture size should be less than ' + maxUploadSize / 1000 + 'kB')
                return
            }

            var form_data = new FormData()
            //Take the first selected file
            form_data.append( "avatar", files[ 0 ] )
            form_data.append( "MAX_FILE_SIZE", maxUploadSize ) // 300k

            $http.post(
            '/api/index.php?/users/avatar',
            form_data, {
                headers: {
                    'Content-Type': undefined,
                },
                transformRequest: angular.identity
            })
            .then(function(response) {
                // console.log(response)
                $rootScope.user_data.picture = response.data.avatar
            })
            .catch(function(error) {
                console.error(error)
                armSystemService.showMessage('There was an error while uploading the image')
            })
        }
    }]
});
