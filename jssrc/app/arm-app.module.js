/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

// Define the `armApp` module
angular
.module('armApp', [
    // `armApp` module dependencies
    'ngRoute',

    'armServices',

    'armAdmin',
    'armCommonModule',
    'armAppAuth',
    'armApartment',

    'armSandbox'
]);
