/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

angular
.module('armApartment')
.directive('armApartmentFilter',
['$routeParams', 'armApartmentsService',
function( $routeParams, armApartmentsService ) {
    return {
        restrict: 'E',
        templateUrl: 'js/app/apartment/apartment-filter.template.html',
        scope: {
            filter: '<',
            onApplyFilter: '<',
        },
        controller: function() {
            this.doFilter = function() {
                console.log('doFilter!!!!')
            }
        },
        link: function( scope, element, attrs ) {
            var self = this

            if( scope.onApplyFilter ) {
                scope.onApplyFilter()
            }

            console.log('scope.filter', scope.filter)
        }
    }
}]);
