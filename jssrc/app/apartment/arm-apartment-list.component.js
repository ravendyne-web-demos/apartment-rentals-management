/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

angular
.module('armApartment')
.component('armApartmentList', {

    templateUrl: 'js/app/apartment/apartment-list.template.html',
    controller: [ '$location', '$scope', '$rootScope', 'armApartmentsService', 'armSystemService',
    function ApartmentListController($location, $scope, $rootScope, armApartmentsService, armSystemService) {
        var self = this

        $scope.apartmentLatLonList = {}
        $scope.apartmentsFilter = {
            size_from: '',
            size_to: '',
            price_from: '',
            price_to: '',
            rooms_from: '',
            rooms_to: '',
        }
        self.filter = {}
        self.apartments = []

        let updateGeoLocationList = function() {
            var apartmentList = []

            for( var idx in self.apartments ) {

                var apt = self.apartments[ idx ]

                apartmentList.push({
                    lat: apt.geolocation_lat,
                    lon: apt.geolocation_long,
                    name: apt.name,
                })
            }

            $scope.apartmentLatLonList = apartmentList
        }

        this.onApplyFilter = function( filter ) {
            angular.extend( self.filter, filter )
            loadApartments()
        }

        this.newApartment = function() {
            $location.path('/apartment-detail/0')
        }

        this.showApartment = function( apartmentId ) {

            var user_data = $rootScope.user_data
            if( user_data.id === undefined ) return;

            // console.log('apt-list user_data', user_data)
            if( user_data.is_client ) {
                $location.path('/apartment-view/' + apartmentId)
            } else {
                $location.path('/apartment-detail/' + apartmentId)
            }
        }

        let loadApartments = function() {

            if( $rootScope.user_data.is_realtor ) {
                self.filter.realtor_id = $rootScope.user_data.id
            } else {
                self.filter.realtor_id = undefined
            }

            armApartmentsService.getAllApartments( self.filter )
            .then(function(apartments) {
                // console.log('apartments', apartments)
                self.apartments = apartments
                // console.log('apartments',apartments)
                updateGeoLocationList()
            })
            .catch(function(error) {
                if(error.status && error.status == 404) {
                    self.apartments = []
                    updateGeoLocationList()
                    return
                }
                armSystemService.showMessage('Error while getting apartment list')
            })
        }

        this.deleteApartment = function( apartmentId ) {

            if( ! confirm('Delete the apartment?') ) {
                return
            }

            // console.log('delete apartment')

            armApartmentsService
            .deleteApartment( apartmentId )
            .then(function(response) {
                loadApartments()
            })
            .catch(function(error) {
                msg = 'Error while trying to delete this apartment'
                armSystemService.showMessage(msg)
            })
        }

        this.rentApartment = function( apartmentId ) {

            apartment_data = {
                is_rented: true,
            }

            armApartmentsService
            .updateApartment( apartmentId, apartment_data )
            .then(function(response) {
                loadApartments()
            })
            .catch(function(error) {
                msg = 'Error while trying to mark this apartment as rented'
                armSystemService.showMessage(msg)
            })
        }

        loadApartments()
    }]
});
