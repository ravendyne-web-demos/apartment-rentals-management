/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

angular
.module('armApartment')
.component('armApartmentFilter', {

    templateUrl: 'js/app/apartment/apartment-filter.template.html',

    bindings: {
        apartmentFilter: '<',
        onApplyFilter: '<',
    }, 

    controller: function() {
        var self = this

        this.doFilter = function() {
            // console.log('self.apartmentFilter', self.apartmentFilter)

            if( self.onApplyFilter ) {
                self.onApplyFilter( self.apartmentFilter )
            }
        }
    }
});
