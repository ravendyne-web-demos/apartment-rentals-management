/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

angular
.module('armApartment')
.component('armApartmentView', {

    templateUrl: 'js/app/apartment/apartment-view.template.html',
    controller: ['$routeParams', '$scope', 'armApartmentsService',
    function ApartmentDetailController($routeParams, $scope, armApartmentsService) {
        var self = this
        var apartmentId = $routeParams.apartmentId

        $scope.apartmentLatLon = {}

        let updateGeoLocation = function() {
            $scope.apartmentLatLon = {
                lat: self.apartment.geolocation_lat,
                lon: self.apartment.geolocation_long,
                name: self.apartment.name,
            }
        }

        armApartmentsService
        .getApartment( apartmentId )
        .then(function(response) {
            // console.log(response)
            self.apartment = response
            updateGeoLocation()
        })
    }]
});
