/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

angular
.module('armApartment')
.component('armApartmentDetail', {

    templateUrl: 'js/app/apartment/apartment-detail.template.html',
    controller: ['$rootScope', '$scope', '$location', '$routeParams', '$timeout', 'armApartmentsService', 'armUsersService', 'armSystemService',
    function ApartmentDetailController($rootScope, $scope, $location, $routeParams, $timeout, armApartmentsService, armUsersService, armSystemService) {
        var self = this
        var apartmentId = $routeParams.apartmentId

        self.update_mode = false
        if( apartmentId && apartmentId > 0 ) {
            self.update_mode = true
        }
        self.all_realtors = []
        $scope.apartmentLatLon = {}

        let roundToSixDecimals = function( value ) {
            return Number( Math.round( value + 'e6' ) + 'e-6' )
        }

        let updateGeoLocation = function() {
            $scope.apartmentLatLon = {
                lat: roundToSixDecimals( self.apartment.geolocation_lat ),
                lon: roundToSixDecimals( self.apartment.geolocation_long ),
                name: self.apartment.name,
            }
        }

        self.updateLocationFromMap = function() {
            // use $timeout to trigger angularjs to process
            // ng-model bindings (among other things)
            // $timeout uses $apply() internaly and by using $timeout
            // instead of $apply() we avoid random "$digest already in progress ..." errors
            $timeout(function() {
                self.apartment.geolocation_lat = roundToSixDecimals( $scope.apartmentLatLon.lat )
                self.apartment.geolocation_long = roundToSixDecimals( $scope.apartmentLatLon.lon )
            })
        }

        let getRealtor = function( user_id ) {

            for( var i = 0; i < self.all_realtors.length; i++ ) {

                if( user_id == self.all_realtors[i].id ) {

                    return self.all_realtors[i]
                }
            }
        }

        let normalizeAssociatedRealtor = function() {

            let normalized_apt = angular.copy( self.apartment )

            if( normalized_apt.associated_realtor.id ) {
                normalized_apt.associated_realtor = normalized_apt.associated_realtor.id
            }

            return normalized_apt
        }

        let updateFromSelectedRealtor = function() {

            for( var i = 0; i < self.all_realtors.length; i++ ) {

                if( self.apartment.associated_realtor == self.all_realtors[i].id ) {

                    self.apartment.associated_realtor = self.all_realtors[i]
                    return
                }
            }
        }

        this.addApartment = function() {

            let apartment_data = normalizeAssociatedRealtor()
            apartment_data.realtor_id = apartment_data.associated_realtor
            console.log('apartment_data', apartment_data)

            armApartmentsService
            .createApartment( apartment_data )
            .then(function(response) {
                console.log('create-apartment', response)
                // go to apartment list
                $location.path('/apartment-list')
            })
            .catch(function(error) {
                msg = 'Error while creating'
                if( error.status == 404 ) {
                    msg = 'Nothing created'
                }
                armSystemService.showMessage(msg)
            })
        }

        this.updateApartment = function() {

            let apartment_data = normalizeAssociatedRealtor()
            console.log('apartment_data', apartment_data)

            armApartmentsService
            .updateApartment( apartmentId, apartment_data )
            .then(function(response) {
                console.log('update-apartment', response)
                // go to apartment list
                $location.path('/apartment-list')
            })
            .catch(function(error) {
                msg = 'Error while updating'
                if( error.status == 404 ) {
                    msg = 'Nothing updated'
                }
                armSystemService.showMessage(msg)
            })
        }

        armUsersService
        .getAllRealtors()
        .then(function( all_realtors ) {

            console.log( 'all_realtors', all_realtors )
            self.all_realtors = all_realtors

            if( self.update_mode ) {

                armApartmentsService
                .getApartment( apartmentId )
                .then(function(response) {
                    // console.log(response)
                    self.apartment = response
                    updateFromSelectedRealtor()
                    updateGeoLocation()
                })

            } else {

                $.getJSON('http://ip-api.com/json', function(data) {

                    $timeout(function() {
                        self.apartment.name = ''
                        self.apartment.geolocation_lat = roundToSixDecimals( data.lat )
                        self.apartment.geolocation_long = roundToSixDecimals( data.lon )
                        updateGeoLocation()
                    })
                });

                self.apartment = {
                    associated_realtor: getRealtor( $rootScope.user_data.id ),
                }
            }
        })
    }]
});
