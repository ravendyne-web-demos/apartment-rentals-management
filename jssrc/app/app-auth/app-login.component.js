/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

angular
.module('armAppAuth')
.component('armAppLogin', {

    templateUrl: 'js/app/app-auth/app-login.template.html',

    controller: [ '$location', '$rootScope', 'armAuthService', 'armSystemService',
    function AppLoginController($location, $rootScope, armAuthService, armSystemService) {
        var self = this
        self.loginIdentity = ''
        self.loginPassword = ''

        $('html').addClass('login')

        this.doLogin = function() {

            $rootScope.user_data = undefined

            var id_pass = {
                'identity' : self.loginIdentity,
                'password' : self.loginPassword
            }

            // console.log( 'id_pass', id_pass )

            armAuthService.login( id_pass )
            .then(function(user_data) {
                // console.log(user_data)
                // navigate to dash which then decides what we do next
                $location.path('/dash')
                $('html').removeClass('login')
            })
            .catch(function(error) {
                // console.error( 'login error response', error )
                armSystemService.showMessage('Login failed')
            })
    
        }
    }]
});

angular
.module('armAppAuth')
.component('armOauth2Login', {

    template: '<div>Processing...</div>',

    controller: [ '$routeParams', 'armAuthService',
    function ($routeParams, armAuthService) {

        // this will redirect browser, no promises, no callbacks
        armAuthService.oauth2_login( $routeParams.provider )
    }]
})
