/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

angular
.module('armAppAuth')
.component('armAppRegister', {

    templateUrl: 'js/app/app-auth/app-register.template.html',

    controller: [ '$location', '$routeParams', 'armAuthService', 'armSystemService',
    function RegisterUserController( $location, $routeParams, armAuthService, armSystemService ) {
        self = this
        self.registration_data = {
            send_email: true
        }
        if( $routeParams.email ) {
            self.registration_data.email = $routeParams.email
            self.registration_data.send_email = false
        }
        // console.log('self.registration_data', self.registration_data)

        this.doRegister = function() {
            console.log('self.registration_data', self.registration_data)

            if( ! ( self.registration_data.username || self.registration_data.email || self.registration_data.password || self.registration_data.password_repeat ) ) {
                armSystemService.showMessage('All fields are required')
                return
            }

            if( self.registration_data.password != self.registration_data.password_repeat ) {
                armSystemService.showMessage('Passwords must match')
                return
            }

            armAuthService
            .register( self.registration_data )
            .then(function(response) {
                console.log('registration response', response)
                $location.path('/registration-success' + ( self.registration_data.send_email ? '' : '/no_confirmation' ) )
            })
            .catch(function(error) {
                console.error('error', error)

                if( error.data && error.data.message ) {
                    armSystemService.showMessage(error.data.message)
                    return
                }

                armSystemService.showMessage('There was an error during registration')
            })

        }
    }]
});


angular
.module('armAppAuth')
.component('armAppRegistrationSuccess', {

    templateUrl: 'js/app/app-auth/app-registration-success.template.html',

    controller: [ '$routeParams', 
    function RegisterUserController( $routeParams ) {
        this.confirmation_required = true
        if( $routeParams.noConfirmation ) {
            this.confirmation_required = false
        }
    }]
});
