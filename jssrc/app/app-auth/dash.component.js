/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

angular
.module('armAppAuth')
.component('armDash', {

    template: 'Loading...',

    controller: [ '$location', '$rootScope', 'armAuthService', 'armSystemService',
    function DashController($location, $rootScope, armAuthService, armSystemService) {
        let self = this

        armAuthService.whoAmI()
        .then(function(user_data) {
            // console.log(response)

            // console.log('user_data', user_data)
            $rootScope.user_data = user_data
            if( user_data.is_admin ) {
                $location.path('/admin-dash')
            } else {
                $location.path('/apartment-list')
            }
        })
        .catch(function(error) {
            // show error
            console.error( 'error response', error )
            armSystemService.showMessage('Unexpeced error popped up...')
        })
    }]
});
